﻿using UnityEngine;
using System.Collections;

public class CheckPoint : MonoBehaviour {

	public LevelManager levelManager;

	// Use this for initialization
	void Start () {
		//finds any objects that are in your scene that are attached to the LevelManager Script
		levelManager = FindObjectOfType<LevelManager> ();
	}
	
	void OnTriggerEnter2D(Collider2D other){
		if (other.name == "Player") {

			//the current checkpoint the player gameobject interacts with will be assigned as the new checkPoint
			levelManager.currentCheckPoint = gameObject;		
		}
	}
}
