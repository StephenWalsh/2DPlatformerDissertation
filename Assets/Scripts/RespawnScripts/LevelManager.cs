﻿using UnityEngine;
using System.Collections;

public class LevelManager : MonoBehaviour {

	//LevelManager variables
	public GameObject currentCheckPoint;
	public int deathCost;
	private PlayerX player;


	void Start () {
		player = FindObjectOfType<PlayerX> ();
	}//Start()

	//method to respawn the player to the last checkpoint reached
	public void RespawnPlayer(){
		Debug.Log ("Player Respawn Check");
		player.transform.position = currentCheckPoint.transform.position;
		ScoreManager.AddPoints (-deathCost);
	}//RespawnPlayer
}//LevelManager
