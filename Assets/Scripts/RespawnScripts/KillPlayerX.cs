﻿using UnityEngine;
using System.Collections;

public class KillPlayerX : MonoBehaviour {


	public LevelManager levelManager;
	public GameObject respawnParticle;
	public Transform checkPoint;

	//finds any objects that are in your scene that are attached to the LevelManager Script
	void Start () {
		levelManager = FindObjectOfType<LevelManager> ();
	}//Start()
	
	//Activates the respawnPlayer method within the levelManager script
	void OnTriggerEnter2D(Collider2D other){
		if (other.name == "Player") {
			Instantiate (respawnParticle, checkPoint.position, checkPoint.rotation);
			levelManager.RespawnPlayer();		
		}//if
	}//OnTriggerEnter2D()
}//KillPlayerX
