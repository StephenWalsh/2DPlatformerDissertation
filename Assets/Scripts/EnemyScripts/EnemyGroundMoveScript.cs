﻿using UnityEngine;
using System.Collections;

public class EnemyGroundMoveScript : MonoBehaviour {

	//EnemyGroundMoveScript variables
	public float moveSpeed;
	public bool moveRight;

	public Transform wallCheck;
	public float wallCheckRadius;
	public LayerMask whatisWall;
	private bool hittingWall;

	public Transform edgeCheck;
	private bool hittingEdge;


	// Update is called once per frame
	void Update () {

		//change direction when wall is hit using the circle raycast
		hittingWall = Physics2D.OverlapCircle (wallCheck.position, wallCheckRadius, whatisWall);

		//change direction when edge is found using the circle raycast
		hittingEdge = Physics2D.OverlapCircle (edgeCheck.position, wallCheckRadius, whatisWall);

		//if wall is detected or edge is found change direction
		if (hittingWall || !hittingEdge)
			moveRight = !moveRight;

		//when velocity reverses mirror the transform of the image along the x-axis
		if (moveRight) {
			transform.localScale = new Vector3(-1f, 1f,1f);
			GetComponent<Rigidbody2D>().velocity = new Vector2(moveSpeed, GetComponent<Rigidbody2D>().velocity.y);
		}//if
		else{
			transform.localScale = new Vector3(1f, 1f,1f);
			GetComponent<Rigidbody2D>().velocity = new Vector2(-moveSpeed, GetComponent<Rigidbody2D>().velocity.y);
		}//else

	}//Update()

}//EnemyGroundMoveScript