﻿using UnityEngine;
using System.Collections;

public class EnemyStarcontroller : MonoBehaviour {

	//enemy start controller
	public float speed;
	public Player player;
	public GameObject enemyDeathEffect;
	public GameObject impactEffect;
	public int pointsForKill;
	public float rotationSpeed;
	public int damageToGive;
	private Rigidbody2D myrigidbody2D;
	public LayerMask whatToHit;


	//Start method set up references
	void Start () {
		player = FindObjectOfType<Player> ();
		myrigidbody2D = GetComponent<Rigidbody2D> ();
		//fires the projectile in the direction of where the player character is located in respect to the enemy
		if (player.transform.position.x<transform.position.x){
			speed = -speed;
			Vector3 theScale = transform.localScale;
			rotationSpeed = -rotationSpeed;
		}//if
	}//Start()

	// Update is called once per frame
	void Update () {
		myrigidbody2D.velocity = new Vector2 (speed, myrigidbody2D.velocity.y);
		myrigidbody2D.angularVelocity = rotationSpeed;
	}//Update()

	//make the enemy star/bullet to do damage, interact or destroy with gameobjects with the following tags
	void OnTriggerEnter2D(Collider2D other){
		if (other.name == "Player") {
			player.DamagePlayer(damageToGive);
			Instantiate(impactEffect, transform.position, transform.rotation);
			Destroy(gameObject);
		}//if
		if (other.tag == "box") {
			Instantiate(impactEffect, transform.position, transform.rotation);
			Destroy(gameObject);
		}//if
		if (other.tag == "ally") {
			Instantiate(impactEffect, transform.position, transform.rotation);
			Destroy(gameObject);
		}//if
		if (other.tag == "Ground") {
			Instantiate(impactEffect, transform.position, transform.rotation);
			Destroy(gameObject);
		}//if
	}//OnTriggerEnter2D

}//EnemyStarController
