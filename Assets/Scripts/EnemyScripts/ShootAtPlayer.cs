﻿using UnityEngine;
using System.Collections;

public class ShootAtPlayer : MonoBehaviour {

	//ShootAtPlayer variables
	public float playerInRange;
	public GameObject enemyStar;
	public PlayerX player;
	public Transform launchPoint;
	public float waitBetweenShots;
	private float shotCounter;
	public LayerMask whatToHit;


	// set up PlayerX references - set up a shot counter countdown to wait between each shot
	void Start () {
		player = FindObjectOfType<PlayerX>();
		shotCounter = waitBetweenShots;
	}//Start()
	
	// Update is called once per frame
	void Update () {
		//DrawLine on either side of the enemy of a distance playerInRange
		Debug.DrawLine (new Vector3(transform.position.x - playerInRange, transform.position.y, transform.position.z),
		                new Vector3(transform.position.x + playerInRange, transform.position.y, transform.position.z));
	
		//only if shot counter reaches 0
		shotCounter -= Time.deltaTime;

		//if player is range instantiate the enemystar/enemybullet
		if (player.transform.position.x > transform.position.x && 
		    player.transform.position.x < transform.position.x + playerInRange && shotCounter<0) {
			Instantiate(enemyStar, launchPoint.position, launchPoint.rotation);
			shotCounter = waitBetweenShots;
		}//if

		//if player is range instantiate the enemystar/enemybullet on other side of enemy
		if (transform.localScale.x > 0 && player.transform.position.x < transform.position.x && 
		    player.transform.position.x > transform.position.x - playerInRange && shotCounter<0) {
			Instantiate(enemyStar, launchPoint.position, launchPoint.rotation);
			shotCounter = waitBetweenShots;
		}//if



	}//Update()
}//ShootAtPlayer
