﻿using UnityEngine;
using System.Collections;

public class GiveDamageToPlayer : MonoBehaviour {

	/// The amount of health to remove from the player's health
	public int DamageToGive = 10;
	public bool dontKnockback;
	private Vector2 lastPosition,velocity;
	
	/// During last update, we store the position and velocity of the object
	public void LateUpdate () {
		velocity = (lastPosition - (Vector2)transform.position) /Time.deltaTime;
		lastPosition = transform.position;
	}//LateUpdate()

	/// When a collision with the player is triggered, we give damage to the player and knock it back
	public void OnTriggerEnter2D(Collider2D other){
		if (other.name == "Player") {
			var player= other.GetComponent<Player>();
			player.DamagePlayer(DamageToGive);

			//if dontknockback is false, then knockback the player from the PlayerX script by a distance of knockbackLength
			if(dontKnockback==false){
			var playerKnockback = other.GetComponent<PlayerX>();
			playerKnockback.knockbackCount = playerKnockback.knockbackLength;
			
			if(other.transform.position.x < transform.position.x){
				playerKnockback.knockFromRight = true;
			}//if
			//otherwise don't knockback player i.e. for health and mana pickups
			else{
				playerKnockback.knockFromRight = false;
			}//else
		}//if
	}//if

	//if no player is not found on collision, just return
	if (other.tag!="Player")
			return;
	}//if
}//giveDamageToPlayer
