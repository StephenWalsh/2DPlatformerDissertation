﻿using UnityEngine;
using System.Collections;

[RequireComponent (typeof (Controller2D))]
public class PlayerX : MonoBehaviour {

	//Jumping variables
	public float maxJumpHeight = 4f;
	public float minJumpHeight = 1f;
	public float timeToJumpApex = .4f;
	public float accelerationTimeAirborne = .2f;
	public float accelerationTimeGrounded = .1f;
	public float moveSpeed = 10f;

	//different wall jumping variables
	public Vector2 wallJumpOff;
	public Vector2 wallJumpClimb;
	public Vector2 wallJumpLeap;

	//wall sticking/sliding variables
	public float wallStickTime = .25f;
	float timeToWallUnstick;
	public float wallSlideSpeedMax = 3;
	int jumpCount = 0;

	//knockback variables when the player collides with an enemy
	public float knockback;
	public float knockbackLength;
	public float knockbackCount;
	public bool knockFromRight;

	//climb ladders/levitators variables
	public bool onLadder;
	public float climbSpeed;
	private float climbVelocity;
	
	//Used to set gravity to zero so player can freely movely upwards and downwards on a ladder/levitator
	private float gravityStore;

	//player unique gravitational variable along with various velocities related to player movement
	float gravity;
	float maxJumpVelocity;
	float minJumpVelocity;
	Vector3 velocity;
	float velocityXSmoothing;
	bool candoublejump = false;

	//Reads in the input from the Controller2D script and applies it to all variables in this class
	Controller2D controller;
	

	// These are markings to show and check if player is grounded or hitting a ceiling of sorts
	private Transform groundCheck; 
	private float groundedRadius = .2f; 
	private bool grounded = false; 
	private Transform ceilingCheck; 
	private float ceilingRadius = .01f; 
	private Animator anim; 
	
	//new variable so we can can change direction
	public Transform playerGraphics;

	// For determining which way the player is currently facing.
	private bool facingRight = true; 

	// Whether or not a player can steer while jumping;
	[SerializeField] private bool airControl = false; 

	// A mask determining what is ground to the character
	[SerializeField] private LayerMask whatIsGround; 



	//Start method to assign the player's unique gravitational and velocity variables 
	void Start() {

		//Gets the componant from the Controller2D input class and assign it
		controller = GetComponent<Controller2D> ();

		//Assign the animator componant to allow the player to be animated
		anim = GetComponent<Animator>();

		//the gravitational force respect to the maximum jumping velocity
		gravity = -(2 * maxJumpHeight) / Mathf.Pow (timeToJumpApex, 2);
		maxJumpVelocity = Mathf.Abs(gravity) * timeToJumpApex;
		minJumpVelocity = Mathf.Sqrt (2 * Mathf.Abs (gravity) * minJumpHeight);
		print ("Gravity: " + gravity + "  Jump Velocity: " + maxJumpVelocity);

		// Setting up references to check if the player collides with the ground or ceiling
		groundCheck = transform.Find("GroundCheck");
		ceilingCheck = transform.Find("CeilingCheck");

		//if there are no playergraphics assign, throw up an error within the Unity console
		if (playerGraphics == null) {
			
			Debug.LogError("there is no graphics object as a child of the player");
		}//if
	}//Start()


	//fixed update checks for player movement and animation and ground movement 
	private void FixedUpdate(){

		// The player determines what is ground from the circlecast 
		//which checks groundcheck position i.e. anything designated as the ground
		grounded = Physics2D.OverlapCircle(groundCheck.position, groundedRadius, whatIsGround);
		anim.SetBool("Ground", grounded);

		//assign the gravityStore temporary variable to be assigned to the gravity
		gravityStore = gravity;

		// Set the vertical animation
		anim.SetFloat("vSpeed", velocity.y);
	}//FixedUpdate()


	//Be ready to accept the controller input at any time
	void Update() {

		//[Future graphical update for combat and pickup text]
		if (Input.GetKeyDown (KeyCode.Q)) {
			CombatText.Instance.CreateText(transform.position);		
		}//if

		int wallDirX = (controller.collisionData.left) ? -1 : 1;

		//Be ready to accept the controller input at any given time
		Move (Input.GetAxisRaw ("Horizontal"));
		Vector2 input = new Vector2 (Input.GetAxisRaw ("Horizontal"), Input.GetAxisRaw ("Vertical"));
		float targetVelocityX = input.x * moveSpeed;

		//if the player is currently not being knockbacked by an enemy allowed for user input
		if (knockbackCount <= 0) {

			//there is a horizontal input from left or right, set the velocity the targetVelocityX dictates the direction and smoothing
			//if there is ground (i.e. collisions below) apply the ground acceleration otherwise apply the acceleration airborne
			velocity.x = Mathf.SmoothDamp (velocity.x, targetVelocityX, ref velocityXSmoothing, 
			                               (controller.collisionData.below)?accelerationTimeGrounded:accelerationTimeAirborne);
			//if there is any horizonatal input activate the player run animation, otherwise play the player idle Animation
			if(Mathf.Abs(Input.GetAxisRaw ("Horizontal"))> 0.1){
				anim.Play("PlaerRun");
			}//if
			else{
				//if user is not running
				anim.Play("PlayerIdleAnim");
			}//else
		}//if

		//else knockback is true, if collision from right move to the left otherwise move to the right and set knockback count time to 0
		else{
			if(knockFromRight){
				velocity.x = -knockback;
				velocity.y = minJumpVelocity;
			}//if
			if(!knockFromRight){
				velocity.x = knockback;
				velocity.y = minJumpVelocity;
			}//if
			knockbackCount -= Time.deltaTime;
		}//else


		//update variable to always assign wall sliding to be false
		bool wallSliding = false;
		//if collisions from left or right (i.e. wall collision) when airborne, then wall sliding will be true
		if ((controller.collisionData.left || controller.collisionData.right) && !controller.collisionData.below && velocity.y < 0) {
			wallSliding = true;

			//assigns velocity.y to the max wall slide speed
			if (velocity.y < -wallSlideSpeedMax) {
				velocity.y = -wallSlideSpeedMax;
			}//if

			//if player doesn't want to wall slide the unstick after moving away from wall for a short time
			if (timeToWallUnstick > 0) {

				//make sure x velocity doesn't change when on wall
				velocityXSmoothing = 0;
				velocity.x = 0;

				//if controller input is going opposite to the wall, the player will fall off the wall, else will stay on the wall
				if (input.x != wallDirX && input.x != 0) {
					timeToWallUnstick -= Time.deltaTime;
				}//if
				else {
					timeToWallUnstick = wallStickTime;
				}//else
			}//if
			else {
				timeToWallUnstick = wallStickTime;
			}//else
			
		}

		//if collisions detected above or below stop the y acceleration
		if (controller.collisionData.above || controller.collisionData.below) {
			velocity.y = 0;
		}//if
		

		//if user presses the space bar - Player jumping and wall jumping
		if (Input.GetKeyDown (KeyCode.Space) ) {

			//if player is on wall (i.e. wall sliding) and jumps the right direction to the wall, then climb wall
			if (wallSliding) {
				if (wallDirX == input.x) {
					velocity.x = -wallDirX * wallJumpClimb.x;
					velocity.y = wallJumpClimb.y;
				}//if
				//no horizontal input and jump, player just jumps off the wall at specific x and y vel.
				else if (input.x == 0) {
					velocity.x = -wallDirX * wallJumpOff.x;
					velocity.y = wallJumpOff.y;
				}//else if
				//else player is on wall (i.e. wall sliding) and jumps the opposite direction of the wall, then jump away from wall
				else {
					velocity.x = -wallDirX * wallJumpLeap.x;
					velocity.y = wallJumpLeap.y;
				}//else

			}//if

			//if player is grounded, velocity is assigned to max jump velocity and at this point the player can double jump
			if (controller.collisionData.below) {
				velocity.y = 0;
				velocity.y = maxJumpVelocity;
				candoublejump = true;

			}//if
			else {
				//if canDoubleJump is true player is allowed to jump again with the minimum jump velocity
				if (candoublejump){

					candoublejump = false;
				velocity.y = 0;
				velocity.y = minJumpVelocity;
				}//if
			}//else
		}//if

		//if at any stage the spacebar is lifted during a jump the player can jump anywhere in between the minimum and maximum jump velocity
		//this gives an added control to the player and stems away from a control system scheme
		if( Input.GetKeyUp(KeyCode.Space)){
			if(velocity.y > minJumpVelocity){
			velocity.y = minJumpVelocity;
			}//if
		}//if

		//call jump method
		Jump ();


		//If the input is moving the player right and the player is facing left...
		if (Input.GetAxisRaw ("Horizontal") > 0 && !facingRight){
			// ... flip the player along x axis
			Flip ();
		}//if 
		// Otherwise if the input is moving the player left and the player is facing right...
		else if (Input.GetAxisRaw ("Horizontal") < 0 && facingRight){
			// ... flip the player along x axis
			Flip ();
		}//else if


		//when player uses a spell attack then activate the spell animation when mouse0 button is pressed,
		//then instantly stop animation afterwards
		if (anim.GetBool("Spell")) {
			anim.SetBool("Spell", false);
		}//if
		if (Input.GetKeyDown (KeyCode.Mouse0)) {
			anim.PlayInFixedTime("PlayerAttackAnim");
			anim.SetBool("Spell", true);		
		}//if


		//if on ladder assign the gravity to be zero using the gravityStore temporary variable
		//This allows the user to move up and down via vertical input from the keyboard with the climb velocity
		if (onLadder) {
			gravityStore = 0f;
			climbVelocity = climbSpeed * Input.GetAxisRaw("Vertical");
			velocity = new Vector2 (velocity.x, climbVelocity);
		}//if

		//if not on ladder re-assign the gravity store to the original gravity again 
		if (!onLadder) {
			gravityStore = gravity;
		}//if
	
	}//Update()



	//Flip method to flip the player respective to his or her directionaal input
	private void Flip()	{
		// Switch the way the player is labelled as facing.
		facingRight = !facingRight;
		
		// Multiply the player's x local scale by -1.
		Vector3 theScale = playerGraphics.localScale;
		theScale.x *= -1;
		playerGraphics.localScale = theScale;
	}//Flip()

//#if CROSS_PLATFORM_INPUT (future mobile and tablet input contols... 


	//TouchScreen Controls [future update in progress]
	public void Move(float moveInput){
		Vector2 input = new Vector2 (moveInput, moveInput);

	}//Move()

	//Jump method assigns the y velocity in respect to acceleration due to gravity
	public void Jump(){
		velocity.y += gravity * Time.deltaTime;
		controller.Move (velocity * Time.deltaTime);
	}//Jump()

}//PlayerX








