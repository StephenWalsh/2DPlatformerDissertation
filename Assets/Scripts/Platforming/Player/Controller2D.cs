﻿using UnityEngine;
using System.Collections;
[RequireComponent (typeof (BoxCollider2D))]


public class Controller2D : RaycastController {
	
	//Controller2D variables:

	//max angles the character can ascend/descend
	float maxClimbAngle = 70;
	float maxDescendAngle = 70;

	//used to store collisions and the data of collisions
	public CollisionInfo collisionData;

	//start method has priority to override methods
	public override void Start() {
		base.Start ();
		collisionData.faceDirection = 1;
	}//Start()

	//move method that references all velocities calculated to move the player, also to descend and ascend slopes
	public void Move(Vector3 velocity, bool standingOnPlatform = false) {
				UpdateRaycastOrigins ();
		collisionData.Reset ();
		collisionData.velocityOld = velocity;

		if (velocity.x != 0) {
			collisionData.faceDirection = (int) Mathf.Sign(velocity.x);
			}//if
				if (velocity.y < 0) {
						DescendSlope (ref velocity);
			}//if
						HorizontalCollisions (ref velocity);
				
				if (velocity.y != 0) {
						VerticalCollisions (ref velocity);
			}//if
		
				transform.Translate (velocity);
		
				if (standingOnPlatform) {
			collisionData.below = true;
			}//if
		
		}//Move

	//method to check for horizontal collisions and slopes
	void HorizontalCollisions(ref Vector3 velocity) {
		float directionX = collisionData.faceDirection;
		float rayLength = Mathf.Abs (velocity.x) + skinWidth;

		if (Mathf.Abs (velocity.x) < skinWidth) {
			rayLength = 2* skinWidth;
		}//if

		//for every raycast of movement (probably be 4) space each out equally and draw red ray to demonstrate the moving force
		for (int i = 0; i < horizontalRayCount; i ++) {
			Vector2 rayOrigin = (directionX == -1)?raycastOrigins.bottomLeft:raycastOrigins.bottomRight;
			rayOrigin += Vector2.up * (horizontalRaySpacing * i);
			RaycastHit2D hit = Physics2D.Raycast(rayOrigin, Vector2.right * directionX, rayLength, collisionMask);
			
			Debug.DrawRay(rayOrigin, Vector2.right * directionX * rayLength,Color.red);
			
			if (hit) {
				if (hit.distance == 0) {
					continue;
				}//if
				
				float slopeAngle = Vector2.Angle(hit.normal, Vector2.up);

				// if bottom raycast is zero i.e. at the floor and slopeAngle is less than or equal to
				//then the collision is true
				if (i == 0 && slopeAngle <= maxClimbAngle) {

					if (collisionData.descendingSlope) {
						collisionData.descendingSlope = false;
						velocity = collisionData.velocityOld;
					}//if

					//set the descending slope collision info to be false
					float distanceToSlopeStart = 0;
					//if the current slope angle is not equal to the old there is a collision
					if (slopeAngle != collisionData.slopeAngleOld) {
						distanceToSlopeStart = hit.distance-skinWidth;
						velocity.x += distanceToSlopeStart * directionX;
					}//if

					//climb slope method references the climbslope velocity and current slopeAngle
					ClimbSlope(ref velocity, slopeAngle);
					velocity.x += (distanceToSlopeStart * directionX);
					}//if

				//if player slopeAngle is greater than the maxClimbAngle or collision.climbingslope is false, then wall collision 
				//player is stopped, there is a collsion
				if (!collisionData.climbingSlope || slopeAngle > maxClimbAngle) {
					velocity.x = (hit.distance - skinWidth) * directionX;
					rayLength = hit.distance;

					//revert y velocity to stop player (x velocity == 0)
					if (collisionData.climbingSlope) {
						velocity.y = Mathf.Tan(collisionData.slopeAngle * Mathf.Deg2Rad) * Mathf.Abs(velocity.x);
					}//if
					
					collisionData.left = directionX == -1;
					collisionData.right = directionX == 1;
				}//if
			}//if
		}//for
	}//HorizontalCollisions
	
	void VerticalCollisions(ref Vector3 velocity) {
		float directionY = Mathf.Sign (velocity.y);
		float rayLength = Mathf.Abs (velocity.y) + skinWidth;

		//for every raycast of movement (probably be 4) space each out equally and draw red ray to demonstrate the moving vertical force
		for (int i = 0; i < verticalRayCount; i ++) {

			//if vertical rays exist apply gravitation pull inopposite direction to bring player back to ground
			Vector2 rayOrigin = (directionY == -1)?raycastOrigins.bottomLeft:raycastOrigins.topLeft;
			rayOrigin += Vector2.right * (verticalRaySpacing * i + velocity.x);
			RaycastHit2D hit = Physics2D.Raycast(rayOrigin, Vector2.up * directionY, rayLength, collisionMask);
			
			Debug.DrawRay(rayOrigin, Vector2.up * directionY * rayLength,Color.red);

			//if vertical collision
			if (hit) {
				
				velocity.y = (hit.distance - skinWidth) * directionY;
				rayLength = hit.distance;
				
				if (collisionData.climbingSlope) {
					velocity.x = velocity.y / Mathf.Tan(collisionData.slopeAngle * Mathf.Deg2Rad) * Mathf.Sign(velocity.x);
				}//if
				
				collisionData.below = directionY == -1;
				collisionData.above = directionY == 1;
			}//if
		}//for
		
		if (collisionData.climbingSlope) {
			float directionX = Mathf.Sign(velocity.x);
			rayLength = Mathf.Abs(velocity.x) + skinWidth;
			Vector2 rayOrigin = ((directionX == -1)?raycastOrigins.bottomLeft:raycastOrigins.bottomRight) + Vector2.up * velocity.y;
			RaycastHit2D hit = Physics2D.Raycast(rayOrigin,Vector2.right * directionX,rayLength,collisionMask);

			//if collision when climbing slope that's not equal to current slope angle calculate velocity wrt x velocity
			if (hit) {
				float slopeAngle = Vector2.Angle(hit.normal,Vector2.up);
				if (slopeAngle != collisionData.slopeAngle) {
					velocity.x = (hit.distance - skinWidth) * directionX;
					collisionData.slopeAngle = slopeAngle;
				}//if
			}//if
		}//if
	}//VerticalCollision()

	//the climb velocity is respect to the slopeAngle
	void ClimbSlope(ref Vector3 velocity, float slopeAngle) {
		float moveDistance = Mathf.Abs (velocity.x);
		float climbVelocityY = Mathf.Sin (slopeAngle * Mathf.Deg2Rad) * moveDistance;

		//if y velocity is less than climb velocity the assign the y velocity to be the new climb velocity
		if (velocity.y <= climbVelocityY) {
			velocity.y = climbVelocityY;
			//x speed is relative to the slope angle
			velocity.x = (Mathf.Cos (slopeAngle * Mathf.Deg2Rad) * moveDistance * Mathf.Sign (velocity.x));
			//update the collision data with approrpiate movement
			collisionData.below = true;
			collisionData.climbingSlope = true;
			collisionData.slopeAngle = slopeAngle;
		}//if
	}//ClimbSlope()

	//the climb velocity is respect to the slopeAngle
	void DescendSlope(ref Vector3 velocity) {
		float directionX = Mathf.Sign (velocity.x);
		Vector2 rayOrigin = (directionX == -1) ? raycastOrigins.bottomRight : raycastOrigins.bottomLeft;
		RaycastHit2D hit = Physics2D.Raycast (rayOrigin, -Vector2.up, Mathf.Infinity, collisionMask);
		
		if (hit) {
			//if y velocity is less than climb velocity the assign the y velocity to be the new climb velocity
			float slopeAngle = Vector2.Angle(hit.normal, Vector2.up);
			if (slopeAngle != 0 && slopeAngle <= maxDescendAngle) {
				if (Mathf.Sign(hit.normal.x) == directionX) {
					if (hit.distance - skinWidth <= Mathf.Tan(slopeAngle * Mathf.Deg2Rad) * Mathf.Abs(velocity.x)) {
						float moveDistance = Mathf.Abs(velocity.x);
						float descendVelocityY = Mathf.Sin (slopeAngle * Mathf.Deg2Rad) * moveDistance;
						velocity.x = Mathf.Cos (slopeAngle * Mathf.Deg2Rad) * moveDistance * Mathf.Sign (velocity.x);
						velocity.y -= descendVelocityY;
						
						collisionData.slopeAngle = slopeAngle;
						collisionData.descendingSlope = true;
						collisionData.below = true;
					}//if
				}//if
			}//if
		}//if
	}//DescendSlope()
	
	
	//illustrates and reads in where collisions are at all times
	//then resets all bools to false after a specfic collision
	public struct CollisionInfo {
		public bool above, below;
		public bool left, right;
		
		public bool climbingSlope;
		public bool descendingSlope;
		public float slopeAngle, slopeAngleOld;
		public Vector3 velocityOld;

		public int faceDirection;

		//Reset method to return the collision info data to return to zero
		public void Reset() {
			descendingSlope = false;
			climbingSlope = false;
			above = below = false;
			left = right = false;

			slopeAngleOld = slopeAngle;
			slopeAngle = 0;
		}//Reset()
	}//CollisionInfo()
	
}//Controller2D