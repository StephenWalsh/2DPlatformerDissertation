﻿using UnityEngine;
using System.Collections;

public class CoinPickup : MonoBehaviour {

	//CoinPickup variables
	public int pointsToAdd;
	public int coinMagnetDistance;

	//once player comes in contact with the coin/gold add appropriate points to add and destroy the coin gameobject 
	void OnTriggerEnter2D(Collider2D other){

		if (other.GetComponent<PlayerX> () == null) {
			return;
		}//if

		ScoreManager.AddPoints (pointsToAdd);
		Destroy (gameObject);
	}//OnTriggerEnter2D

	//if the player is within a certain distance of the coin/gold, continuously move coin towards player at a speed =15
	void Update(){

		if(Vector3.Distance(transform.position, GameObject.FindGameObjectWithTag("Player").transform.position) < coinMagnetDistance)
				transform.position = Vector3.MoveTowards(transform.position, 
			GameObject.FindGameObjectWithTag("Player").transform.position, Time.deltaTime *15);
	}//Update()

}//CoinPickup()
