﻿using UnityEngine;
using System.Collections;

public class FlameSpell : MonoBehaviour {

	//FlameSpell variable (similiar to weapon variables)
	public float speed;
	//particle emitters display an added effect
	private ParticleEmitter particleEmitter = null; 
	private ParticleAnimator particleAnimator = null;
	Transform firePoint;
	public LayerMask whatToHit;
	public int Damage = 10;
	public Transform trail;


	//searches for the children of weapon rather than labelling it by its specified name in Unity
	void Awake () {
		firePoint = transform.FindChild ("FirePoint");

		// Get the particle emitter or add it
		particleEmitter = GetComponent<ParticleEmitter>();
		
		if (!particleEmitter)
			particleEmitter = gameObject.AddComponent<ParticleEmitter>();
		
		// Get the particle animator or add it
		particleAnimator = GetComponent<ParticleAnimator>();
		
		if (!particleAnimator)
			particleAnimator = gameObject.AddComponent<ParticleAnimator>();
	}//Awake()


	//the fire rate is zero in this case as we want a continuous flame i.e. damage to the enemies
	void Update () {
		if (Input.GetButton("Fire1")){
			particleEmitter.emit = true;
			Shoot();
		}//if
		else if (Input.GetButtonUp("Fire1"))
			particleEmitter.emit = false;

	}//Update()



	//Raycasting calculations, see comments below
	void Shoot(){
		//creating new vector2 assigning it to world point for both x and y axis. Translates position 
		//to the mouse to screen coordinates to position in the world and use to create a raycast 
		Vector2 mousePosition = new Vector2 (Camera.main.ScreenToWorldPoint (Input.mousePosition).x, 
		                                     Camera.main.ScreenToWorldPoint (Input.mousePosition).y);
		
		//take firepoint storing that position as a 2d axis i.,e. vector2
		Vector2 firePointPosition = new Vector2 (firePoint.position.x, firePoint.position.y);


		//Raycast won't take 2 arguments.  It takes the origin, "the direction and distance=100" and also give it a layer mask
		RaycastHit2D hit = Physics2D.Raycast (firePointPosition, mousePosition-firePointPosition, 10, whatToHit);
		
		//draws line from firepoint to mouseposition
		Debug.DrawLine (firePointPosition, (mousePosition-firePointPosition), Color.cyan);

		//if the shot collides with anything with the enemyfly or enemy boss, then do the appropriate damage to the enemy
		if (hit.collider != null) {
			Debug.DrawLine(firePointPosition, hit.point, Color.red);
			EnemyFly enemy = hit.collider.GetComponent<EnemyFly>();
			EnemyBoss enemyBoss = hit.collider.GetComponent<EnemyBoss>();

			
			if (enemy != null){
				Debug.Log("You hit " + hit.collider.name + " and did " + Damage + " Damage.");
				enemy.DamageEnemyFly(Damage);
				GetComponent<AudioSource>().Play();
			}//if
			if (enemyBoss != null){
				Debug.Log("You hit " + hit.collider.name + " and did " + Damage + " Damage.");
				enemyBoss.DamageEnemyFly(Damage);
				GetComponent<AudioSource>().Play();
			}//if
	
		}//if

		Vector3 hitPos;
		Vector3 hitNormal;

		//if hit doesn't hit an enemy, let weapon spell go on forever
		RaycastHit2D yes = Physics2D.Raycast (firePointPosition, mousePosition-firePointPosition, 20, whatToHit);

		if (hit.collider == null){
			hitPos = yes.centroid;
			hitNormal = new Vector3 (9999, 9999, 9999);
		}//if
		else{
			hitPos = hit.point;
			hitNormal = hit.normal;
		}//else
		Effect(hitPos, hitNormal);
	}//Shoot()


	public Transform hitPrefab;

	//Effect method spawns the weapon's effects (see comments below)
	void Effect (Vector3 hitPos, Vector3 hitNormal){
		//to spawn something we transform
		//Every object in a scene has a Transform even the line renderer it stores and manipulates the position, rotation and scale of the object.
		FireMouse ();
		//Every object in a scene has a Transform. It's used to store and manipulate the position, rotation and scale of the object.
		//Transform trail = Instantiate (BulletTrialPrefab, firePoint.position, firePoint.rotation) as Transform;
		LineRenderer lr = trail.GetComponent<LineRenderer> ();
		
		if (lr != null) {
			//set some positions.  Initially we need start and endposition		
			lr.SetPosition(0, firePoint.position);
			lr.SetPosition(1, hitPos);
		}//if
		Destroy (trail.gameObject, 0.2f);
		if (hitNormal != new Vector3 (9999, 9999, 9999)){
			Transform hitParticle = Instantiate(hitPrefab, hitPos, Quaternion.FromToRotation(Vector3.right, hitNormal)) as Transform;
			Destroy (hitParticle.gameObject, 1f);
		}//if
	}//Effect()

	public Transform BulletTrialPrefab;


	public void FireMouse(){
		trail = Instantiate (BulletTrialPrefab, firePoint.position, firePoint.rotation) as Transform;
		
	}//FireMouse()

}//FlameSpell







