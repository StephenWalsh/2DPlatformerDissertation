﻿
using UnityEngine;
using System.Collections;

public class Weapon : MonoBehaviour {

	//Weapon variables
	//want the player to hold down the mouse button to fire in intervals
	//but also want there to be intervals between each fire. i.e. rate of fire
	public float fireRate = 0;
	public int Damage = 10;
	Player player;
	//tell us what to hit.  Ideally, not to hit the player but everything else in the scene
	public LayerMask whatToHit;

	//private variables
	float timeToFire = 0;
	Transform firePoint;
	Transform trail;

	//Bullet trial (BT)
	public Transform BulletTrialPrefab;
	public Transform MuzzleFlashPrefab;

	float timeToSpawnEffect = 0;
	float effectSpawnRate = 10;
	public Transform hitPrefab;

	//handles the camera shaking
	public float camShakeAmount = 0.1f;

	//reference to the camera shake class
	CameraShake camShake;

	//searches for the children of weapon rather than labelling it by its specified name in Unity
	void Awake () {
		firePoint = transform.FindChild ("FirePoint");

		//null check is a good error to know what's wrong
		if (firePoint == null) {
			Debug.LogError("no fire point!");
		}//if
	}//Awake()


	void Start(){
		//set up the gameMaster so we can always refer and get the Camera instance (gm)
		//and getComponenet so we can get the cameraShake on that object
		camShake = GameMaster.gm.GetComponent<CameraShake> ();
		if (camShake == null) {
			Debug.LogError("No CameraShake Script found gm object");		
		}//if
	}//Start()
	
	// if the fire rate has counted down to zero the player is allowed to shoot if the mouse0 button is pressed
	void Update () {
	if (fireRate == 0){
		if (Input.GetButtonDown("Fire1")){
		Shoot();
	//mana drain is a future update the developer is currently working on
			player = GetComponent<Player>();
			player.manaDrain(-10);
		}//if
	}//if

	//checks for button is held in and if Time.time is > the timeToFire
	//then it's going to wait until the time to fire is satisfied by the rateOfFire (delay if it's not 1/fireRate)
	 else{
		if ((Input.GetButtonDown("Fire1")) && (Time.time > timeToFire)){
			timeToFire = Time.time + 1/fireRate;
			Shoot();
			}//if
		}//else
	}//Update()



	//Raycasting calculations, see comments below
	void Shoot(){
		//creating new vector2 assigning it to world point for both x and y axis. Translates position 
		//to the mouse to screen coordinates to position in the world and use to create a raycast 
		Vector2 mousePosition = new Vector2 (Camera.main.ScreenToWorldPoint (Input.mousePosition).x, 
		                                     Camera.main.ScreenToWorldPoint (Input.mousePosition).y);
	
		//take firepoint storing that position as a 2d axis i.,e. vector2
		Vector2 firePointPosition = new Vector2 (firePoint.position.x, firePoint.position.y);
	
		//Raycast won't take 2 arguments.  It takes the origin, "the direction and distance=100" and also give it a layer mask
		RaycastHit2D hit = Physics2D.Raycast (firePointPosition, mousePosition-firePointPosition, 100, whatToHit);

		//draws line from firepoint to mouseposition
		Debug.DrawLine (firePointPosition, (mousePosition-firePointPosition), Color.cyan);

		//if the shot collides with anything with the enemyfly or enemy boss, then do the appropriate damage to the enemy
		if (hit.collider != null) {
			Debug.DrawLine(firePointPosition, hit.point, Color.red);
			EnemyFly enemy = hit.collider.GetComponent<EnemyFly>();
			EnemyBoss enemyBoss = hit.collider.GetComponent<EnemyBoss>();

			if (enemy != null){
				Debug.Log("You hit " + hit.collider.name + " and did " + Damage + " Damage.");
				enemy.DamageEnemyFly(Damage);
				GetComponent<AudioSource>().Play();
			}//if
			if (enemyBoss != null){
				Debug.Log("You hit " + hit.collider.name + " and did " + Damage + " Damage.");
				enemyBoss.DamageEnemyFly(Damage);
				GetComponent<AudioSource>().Play();
			}//if
		}//if

		//if hit doesn't hit an enemy, let weapon spell go on forever
		if (Time.time >= timeToSpawnEffect) {
			
			Vector3 hitPos;
			Vector3 hitNormal;
			
			if (hit.collider == null){
				hitPos = (mousePosition - firePointPosition) * 30;
				hitNormal = new Vector3 (9999, 9999, 9999);
			}//if
			else{
				hitPos = hit.point;
				hitNormal = hit.normal;
			}//else
			Effect(hitPos, hitNormal);
			timeToSpawnEffect = Time.time * 1/effectSpawnRate;
			
		}//if
	}//Shoot()

	//Effect method spawns the weapon's effects (see comments below)
	void Effect (Vector3 hitPos, Vector3 hitNormal){
		//to spawn something we transform
		//Every object in a scene has a Transform even the line renderer it stores and manipulates the position, rotation and scale of the object.
		FireMouse ();
		LineRenderer lr = trail.GetComponent<LineRenderer> ();

		if (lr != null) {
			//set some positions.  Initially we need start and endposition		
			lr.SetPosition(0, firePoint.position);
			lr.SetPosition(1, hitPos);
		}//if
		Destroy (trail.gameObject, 0.2f);
		if (hitNormal != new Vector3 (9999, 9999, 9999)){
			Transform hitParticle = Instantiate(hitPrefab, hitPos, Quaternion.FromToRotation(Vector3.right, hitNormal)) as Transform;
			Destroy (hitParticle.gameObject, 1f);
		}//if

		//need to show v prefab 
		//instantiate is an instance, this instance to store in a variable to 
		//effect one instance of a prefrab and not all of them
		Transform clone = Instantiate (MuzzleFlashPrefab, firePoint.position, firePoint.rotation) as Transform;
		//create a cast to make it such that it's a transform
		//make sure it's parented to firepoint
		clone.parent = firePoint;
		float size = Random.Range (0.6f, 0.9f);
		clone.localScale = new Vector3 (10, 10, 10);
		Destroy (clone.gameObject, 0.2f);

		//Shake the Camera
		camShake.Shake (camShakeAmount, 0.1f);
	}//Effect()

	public void FireMouse(){
		trail = Instantiate (BulletTrialPrefab, firePoint.position, firePoint.rotation) as Transform;
	}//FireMouse()



}//Weapon









