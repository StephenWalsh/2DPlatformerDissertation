﻿using UnityEngine;
using System.Collections;

public class LadderInteraction : MonoBehaviour {

	//Ladder/Levitator Interaction variables
	private PlayerX onPlayer;
	public GameObject floaty;

	//if levitator/ladder is on the player
	void Start () {
		onPlayer = FindObjectOfType<PlayerX>();
	}//Start()

	//if player enters ladder activate the onPlayer variable reference in the PlayerX class and floaty
	void OnTriggerEnter2D (Collider2D other){
		if (other.name == "Player") {
			onPlayer.onLadder = true;
			floaty.SetActive (true);
		}//if
	}//OnTriggerEnter2D()

	//if player exits ladder de-activate the onPlayer variable reference in the PlayerX class and floaty
	void OnTriggerExit2D (Collider2D other){
		if (other.name == "Player") {
			onPlayer.onLadder = false;	
			floaty.SetActive (false);
		}//if
	}//OnTriggerExit2D
}//LadderInteraction
