﻿using UnityEngine;
using System.Collections;

public class SwitchWeapons : MonoBehaviour {


	//Switch weapons variables
	public GameObject lightSpell;
	public GameObject lightSpellIcon;

	public GameObject fireSpell;
	public GameObject fireSpellIcon;

	//public GameObject burst;


	//Update method too continuously wait for user input to switch weapons as well as graphically
	void Update () {
	
		//activates the 1st light spell and de-activates others
		if(Input.GetKeyDown("1")){
			fireSpell.SetActive (false);
			fireSpellIcon.SetActive (false);

		//	burst.SetActive (false);
			lightSpell.SetActive (true);
			lightSpellIcon.SetActive (true);

		}//if
		//activates the 2nd spell (fire/flame spell) and de-activates others
		else if(Input.GetKeyDown("2")){
//			burst.SetActive (false);

			lightSpell.SetActive (false);
			lightSpellIcon.SetActive (false);

			fireSpell.SetActive (true);
			fireSpellIcon.SetActive (true);

		}//else if
		//activates 3rd burst spell and de-activates all others
		else if(Input.GetKeyDown("3")){
//			burst.SetActive (true);
			lightSpell.SetActive (false);
			fireSpell.SetActive (false);
		}//else if
	}//Update()
}//SwitchWeapons
