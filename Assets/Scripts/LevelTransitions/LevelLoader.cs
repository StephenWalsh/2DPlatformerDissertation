﻿using UnityEngine;
using System.Collections;

public class LevelLoader : MonoBehaviour {

	public bool playerInZone;
	public string levelToLoad;
	public string levelTag;
	
	// Assign user not to be with the portal's zone
	//Assign the current level
	void Awake () {
		playerInZone = false;
		PlayerPrefs.SetString("CurrentLevel", Application.loadedLevelName);

	}
	
	void Update () {

		if (playerInZone) {
			LoadLevel();		
		}//if

	}//Update()

	//Loads the level and instantly set's the string of the level that will be loaded 
	void LoadLevel(){
				
		PlayerPrefs.SetString ("CurrentLevel", Application.loadedLevelName);

		Application.LoadLevel (levelToLoad);
	}//LoadLevel()

	void OnTriggerEnter2D (Collider2D other){

		if (other.name == "Player"){
			playerInZone = true;
		}//if
	}//OnTriggerEnter2D
}//LevelLoader
