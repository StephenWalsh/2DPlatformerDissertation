﻿using UnityEngine;
using System.Collections;

public class MainMenu : MonoBehaviour {

	//MainMenu Variables
	public string homeWorld1Open;
	public string loadScreen;

	//Reset the score when the user selects a new game
	public void NewGame(){
		Application.LoadLevel (homeWorld1Open);
		PlayerPrefs.SetInt ("PlayerScore", 0);
	}//NewGame()


	//loads the last level the player character was on
	public void LevelSelect(){
		Application.LoadLevel( PlayerPrefs.GetString( "CurrentLevel" ) );
	}//LevelSelect()

	public void QuitGame(){
		Application.Quit ();
	}//QuitGame()

}//MainMenu
