﻿using UnityEngine;
using System.Collections;

public class EnemyBoss : MonoBehaviour {

	//EnemyBoss variables
	public EnemyFlyStats enemyFlyStats = new EnemyFlyStats();
	public GameObject bossPrefab;
	public float minSize;
	public int Damage = 10;
	Player player;

	//EnemyBoss uses the EnemyFlyStats and health system
	[System.Serializable]
	public class EnemyFlyStats{
		public int maxHealth = 40;
		
		private int _curHealth;
		public int curHealth {
			get {return _curHealth;}
			set {_curHealth = Mathf.Clamp(value, 0, maxHealth);}
		}//curHealth
		
		public void Init(){
			curHealth = maxHealth;
		}//Init()
	}//EnemyFlyStats

	[Header("Optional: ")]
	[SerializeField]
	private StatusIndicator statusIndicator;
	
	
	void Start(){
		enemyFlyStats.Init ();
		
		if (statusIndicator != null) {
			statusIndicator.SetHealth(enemyFlyStats.curHealth, enemyFlyStats.maxHealth);
		}//if
	}//Start()

	//if enemy boss has 0 health, spawn 2 enemy bosses with the Enemy health system
	void Update(){
					
		if (enemyFlyStats.curHealth <= 0) {
			Destroy(gameObject);

		if (transform.localScale.y > minSize) {

			//creates 2 enemies when enemy boss dies
			GameObject clone1 = Instantiate(bossPrefab, new Vector3 (transform.position.x + 0.5f, transform.position.y, transform.position.z), 
				                                transform.rotation) as GameObject;		
			
			GameObject clone2 = Instantiate(bossPrefab, new Vector3 (transform.position.x - 0.5f, transform.position.y, 
				                                                         transform.position.z), transform.rotation) as GameObject;	
			
			//now half the size of the boss you just killed
			clone1.transform.localScale = new Vector3(transform.localScale.y * 0.5f, transform.localScale.y * 0.5f, transform.localScale.z);
			clone1.GetComponent<EnemyFly>().enemyFlyStats.curHealth = enemyFlyStats.maxHealth;
			
			clone2.transform.localScale = new Vector3(transform.localScale.y * 0.5f, transform.localScale.y * 0.5f, transform.localScale.z);
			clone2.GetComponent<EnemyFly>().enemyFlyStats.curHealth = enemyFlyStats.maxHealth;
			
			
				if (clone1.GetComponent<EnemyFly>().enemyFlyStats.curHealth <= 0){
					Destroy(gameObject);
				}//if
				if (clone2.GetComponent<EnemyFly>().enemyFlyStats.curHealth <= 0){
					Destroy(gameObject);
				}//if
			}//if
		}//if

		if (statusIndicator != null) {
			statusIndicator.SetHealth(enemyFlyStats.curHealth, enemyFlyStats.maxHealth);
		}//if
		else{
			Destroy(gameObject);
		}//else
	}//Update()

	//Boss gets damaged when attacked by player
	public void DamageEnemyFly(int damage){
				enemyFlyStats.curHealth -= damage;
		}//DamaageEnemyFly()

	//Damage player on contact
	void OnCollisionEnter2D(Collision2D col){
		if(col.gameObject.name == "Player"){
			Debug.Log("Enemy " +  Damage + " Damage.");
			player.DamagePlayer(Damage);
			//player.transform.position.x -= 2;
		}//if	
	}//OnCollisionEnter2D()
}//EnemyBoss
