﻿using UnityEngine;
using System.Collections;

public class CheckPointActivate : MonoBehaviour {


	//CheckPointActivate variables
	public GameObject DeactivatedCheckpoint;
	public GameObject ActivatedCheckpoint;

	//the graphical representation of the checkpoint being activated
	void OnTriggerEnter2D(Collider2D other){
		if (other.name == "Player") {
			
			DeactivatedCheckpoint.SetActive (false);
			ActivatedCheckpoint.SetActive (true);
		}//if
	}//OnTriggerEnter2D()
}//CheckPointActivate
