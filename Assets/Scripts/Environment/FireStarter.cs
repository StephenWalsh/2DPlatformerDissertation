﻿using UnityEngine;
using System.Collections;

public class FireStarter : MonoBehaviour {

	//FireStarter variables
	public GameObject fire1;
	public GameObject fire2;
	public GameObject fallingBullets;

	//initiates the enemy boss fire on either side of the boss fight
	void OnTriggerEnter2D(Collider2D other){
		
		if (other.tag == "Player") {
			fire1.SetActive(true);
			fire2.SetActive(true);
			fallingBullets.SetActive(true);

		}//if
	}//OnTriggerEnter2D()
}//FireStarter
