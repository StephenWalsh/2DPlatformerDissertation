﻿using UnityEngine;
using System.Collections;

public class FireStopper : MonoBehaviour {

	//FireStopper variables
	public GameObject fire1;
	public GameObject fire2;

	//stops the enemy boss fire on either side of the boss fight
	void OnTriggerEnter2D(Collider2D other){
		
		if (other.tag == "Player") {
			fire1.SetActive(false);
			fire2.SetActive(false);
			
		}//if
	}//OnTriggerEnter2D()
}//FireStopper
