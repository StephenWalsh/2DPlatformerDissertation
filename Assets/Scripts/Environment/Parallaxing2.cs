﻿using UnityEngine;
using System.Collections;

public class Parallaxing2 : MonoBehaviour {

	//Parallaxing2 update from original, can include more than one parallaxed background

	// Array (list) of all the back- and foregrounds to be parallaxed
	public Transform[] paraBackgrounds; 
	// The proportion of the camera's movement to move the backgrounds by
	private float[] parallaxScaling; 
	//how smooth parallaxing will appear in 2D.  0 = not very smooth, 1 = very smooth
	public float smoothing = 1f; 
	private Transform camera; 
	private Vector3 previousCameraPosition; 

	//Is called before Start() to set up camera reference
	void Awake () {
		camera = Camera.main.transform;
	}//Awake()

	//Start method, see comments
	void Start () {
		// The previous frame had the current frame's camera position
		previousCameraPosition = camera.position;
		// assigns the list of all paraBackgrounds and their scalings
		parallaxScaling = new float[paraBackgrounds.Length];
		for (int i = 0; i < paraBackgrounds.Length; i++) {
			parallaxScaling[i] = paraBackgrounds[i].position.z*-1;
		}//for
	}//Start()

	//Update method for how many backgrounds the developer includes 
	void Update () {
		for (int i = 0; i < paraBackgrounds.Length; i++) {
			//parallex must move oppositely to the cameras movement, set a destined x position
			//set up target position which is backgrounds current position with the target x position, fade between these position with the smoothing
			float parallax = (previousCameraPosition.x - camera.position.x) * parallaxScaling[i];
			float backgroundPosX = paraBackgrounds[i].position.x + parallax;
			Vector3 backgroundTargetPos = new Vector3 (backgroundPosX, paraBackgrounds[i].position.y, paraBackgrounds[i].position.z);
			paraBackgrounds[i].position = Vector3.Lerp (paraBackgrounds[i].position, backgroundTargetPos, smoothing * Time.deltaTime);
		}//for

		// set the previousCamPos to the camera's position at the end of the frame
		previousCameraPosition = camera.position;
	}//Update()

}﻿//Parallaxing2