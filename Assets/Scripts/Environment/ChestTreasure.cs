﻿using UnityEngine;
using System.Collections;

public class ChestTreasure : MonoBehaviour {

	//ChestTreasure variables
	public GameObject open;
	public GameObject closed;
	public GameObject GemSaphire;
	public int GemSaphireNum;
	public GameObject GemEmerald;
	public int GemEmeraldNum;
	public GameObject GemRuby;
	public int GemRubyNum;
	public GameObject GemDiamond;
	public int GemDiamondNum;

	//when player interacts with treasure chest it appears open
	//developer can determine how much gold is in each treasure chest in editor
	void OnTriggerEnter2D(Collider2D other){
	
		if (other.tag == "Player") {
			open.SetActive(true);
			closed.SetActive(false);

			//instantiated gold, randomly placed around tresure chest
			for(int i = 0; i<GemSaphireNum; i++){
				Instantiate(GemSaphire, (transform.position + new Vector3(Random.Range(-3.0f,3.0f),Random.Range(-1.0f,3.0f),0f)*
				                         Random.Range(1.0f, 1.0f)), Quaternion.identity);			
			}//for
			for(int i = 0; i<GemEmeraldNum; i++){
				Instantiate(GemEmerald, (transform.position + new Vector3(Random.Range(-3.0f,3.0f),Random.Range(-1.0f,3.0f),0f)*
				                         Random.Range(1.0f, 1.0f)), Quaternion.identity);			
			}//for
			for(int i = 0; i<GemRubyNum; i++){
				Instantiate(GemRuby, (transform.position + new Vector3(Random.Range(-3.0f,3.0f),Random.Range(-1.0f,3.0f),0f)*
				                      Random.Range(1.0f, 1.0f)), Quaternion.identity);			
			}//for
			for(int i = 0; i<GemDiamondNum; i++){
				Instantiate(GemDiamond, (transform.position + new Vector3(Random.Range(-3.0f,3.0f),Random.Range(-1.0f,3.0f),0f)*Random.Range(1.0f, 1.0f))
				            , Quaternion.identity);			
			}//for
		}//if
	}//OnTriggerEnter2D()

	//destroy on exit
	void OnTriggerExit2D(Collider2D other){
		Destroy (gameObject);
	}//OnTriggerExit2D()
}//ChestTreasure
