using UnityEngine;

namespace UnitySampleAssets._2D {

public class Camera2DFollow : MonoBehaviour {

		//Camera2DFollow variables
        public Transform target;
		public float cameraFactor = 3;
		public float cameraReturnSpeed = 0.5f;
		public float cameraMove = 0.1f;
        public float damping = 1;
		public float yboundary = -10;
        private float offsetZ;
        private Vector3 lastTargetPosition;
        private Vector3 currentVelocity;
        private Vector3 lookAheadPos;
		float nextTimeToSearch = 0;

        //sets the last target position as current target position and measures difference of the target and transform position
        private void Start(){
            lastTargetPosition = target.position;
            offsetZ = (transform.position - target.position).z;
            transform.parent = null;
        }//Start()

        // Update method that constant follows the target camera position with the appropriate smoothing/damping 
        private void Update(){
			//once character dies and respawns it is required to find the player again to make the camera follow the player once more
			if (target == null){
				FindPlayer();	
				return;
			}//if

            // only update lookahead pos if accelerating or changed direction
            float xMovement = (target.position - lastTargetPosition).x;
			bool updateLookAheadTarget = Mathf.Abs(xMovement) > cameraMove;

            if (updateLookAheadTarget){
				lookAheadPos = cameraFactor*Vector3.right*Mathf.Sign(xMovement);
            }//if
            else{
				lookAheadPos = Vector3.MoveTowards(lookAheadPos, Vector3.zero, Time.deltaTime*cameraReturnSpeed);
            }//else

			//follows the the target when it moves at a damping
            Vector3 aheadTargetPos = target.position + lookAheadPos + Vector3.forward*offsetZ;
            Vector3 newPos = Vector3.SmoothDamp(transform.position, aheadTargetPos, ref currentVelocity, damping);

			//clamps the camera when target stops and sets the new position and the target position
			newPos = new Vector3 (newPos.x, Mathf.Clamp(newPos.y, -1, Mathf.Infinity), newPos.z);
            transform.position = newPos;
            lastTargetPosition = target.position;
        }//Update()

		//the void method FindPlayer() with tag Player
		void FindPlayer(){
			if (nextTimeToSearch <= Time.time) {
				GameObject searchResult = GameObject.FindGameObjectWithTag("Player");			
				//not found, maintain same search result until so
				if (searchResult != null)
					target = searchResult.transform;
				//search 2 frames per second
				nextTimeToSearch = Time.time + 0.5f;
			}//if
			
		}//FindPlayer()
	}//Camera2DFollow
}//UnitySampleAssets._2D




