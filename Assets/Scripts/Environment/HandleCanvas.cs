﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class HandleCanvas : MonoBehaviour {


	private CanvasScaler scaler;

	// Sets up the UI canvas in respect to the screen size
	void Start () {
		scaler = GetComponent<CanvasScaler> ();
		scaler.uiScaleMode = CanvasScaler.ScaleMode.ScaleWithScreenSize;
	}//Start()

}//HandleCanvas()
