﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class TextBoxMonitor : MonoBehaviour {
	

	//TextBoxMonitor variables
	//gets text file and store each array of strings
	public TextAsset textFile;
	public string[] textLines;
	public GameObject textBox;
	
	public Text theText;
	
	public int currentLine;
	public int endLine;
	
	public PlayerX player;
	public EnemyAI enemy;
	public bool isActive;
	
	public GameObject playerFreeze;
	public GameObject enemyFreeze;

	//animate text variables
	private bool isTyping = false;
	private bool cancelTyping = false;
	public float typeSpeed;
	

	// Awake function locates the notepad.txt file seperates each line and activates and de-activates when necessary
	void Awake () {

		player = FindObjectOfType<PlayerX>();

		if (textFile != null) {
			//grab the test file and seperate each string using the \n
			textLines = (textFile.text.Split('\n'));		
		}//if

		if (endLine == 0) {
			//line numbers start from 1, it reads 1 as position 0.
			endLine = textLines.Length - 1;		
		}//if

		if (isActive) {
			EnableTextBox();

		}//if
		else{
			DisableTextBox();

		}//else
	}//Awake()
	
	//See comments below, update function too large to generalise
	void Update () {

		//if textbox isn't active just return
		if (!isActive) {
			return;		
		}//if
		if (isActive) {
			playerFreeze.GetComponent<PlayerX>().enabled = false;
			enemyFreeze.GetComponent<EnemyAI>().enabled = false;
		}//if

		if(Input.GetKeyDown(KeyCode.Return)){
			//if text is scrolling across screen
			if (!isTyping) {
				currentLine += 1;
				Time.timeScale = 0f;
				//if current line exceeds the endLine entered, then disable textBox
				if (currentLine > endLine) {
					DisableTextBox ();
				}//if
				//otherwise make text scroll across screen
				else{
					StartCoroutine(TextScroll(textLines[currentLine]));
				}//else
			}//if
			else if(isTyping && !cancelTyping){
				cancelTyping = true;
				Time.timeScale = 1f;
			}//else if
		}//if
	}//Update()


	//use coroutine add a letter one after the other
	//corountine work in their own own timeline that it loop to the side
	private IEnumerator TextScroll(string lineOfText){
		int letter = 0;
		//display nothing within the box initially
		theText.text = "";
		//now begin the type scroll
		isTyping = true;
		cancelTyping = false;

		while (isTyping && !cancelTyping && (letter < lineOfText.Length-1)) {
			theText.text += lineOfText[letter];
			//go to next letter
			letter += 1;
			//wait interval between each letter (reason for using coroutine)
			yield return new WaitForSeconds(typeSpeed);
		}//while
		//display the text instant and not take 1 letter at a time
		theText.text = lineOfText;
		isTyping = false;
		cancelTyping = false;
	}//IEnumerator

	//method to activate textbox and activate the text scroll coroutine
	public void EnableTextBox(){
		Time.timeScale = 0f;
		textBox.SetActive (true);		
		isActive = true;

		//goes onto the next line
		StartCoroutine(TextScroll(textLines[currentLine]));
	}//Enabletextbox()

	//method to de-activate textbox and de-freeze the player and specified enemy
	public void DisableTextBox(){
		textBox.SetActive (false);		
		Time.timeScale = 1f;
		isActive = false;
		playerFreeze.GetComponent<PlayerX>().enabled = true;
		enemyFreeze.GetComponent<EnemyAI>().enabled = true;
	}//DisableTextBox()

	//this assigns a different text file into our textBoxMonitor
	public void ReloadScript(TextAsset theText){
		if (theText != null) {
			textLines = new string[1];
			textLines = (theText.text.Split('\n'));
		}//if
	}//ReloadScript()
}//TextBoxMonitor





