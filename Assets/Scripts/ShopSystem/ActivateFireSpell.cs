﻿using UnityEngine;
using System.Collections;

public class ActivateFireSpell : MonoBehaviour {


	//ActivateFireSpell variables
	public int upgradeCost;
	public GameObject upgradeText;
	public GameObject howToUse;

	public bool inZone = false;
	public GameObject fireSpell;
	public GameObject player;
	
	
	// Use this for initialization
	void Start () {
		fireSpell.SetActive (false);
	}//Start
	
	// if player is in zone power up can be activated
	void Update () {
		if (inZone) {
			ActivatePowerup();
		}//if
	}//Update()

	//powerup will only be activated if player is in zone and has a score greater or equal to the upgradecost
	//and subtracts the value appropriately
	void ActivatePowerup(){
		if (Input.GetKeyDown (KeyCode.Return)) {
			
			if(ScoreManager.score > upgradeCost){
				ScoreManager.AddPoints (-upgradeCost);
		
				fireSpell.SetActive (true);
				howToUse.SetActive(true);

				Destroy(gameObject);
			}//if
		}//if
	}//ActivatePowerup()

	//bool set to true if player is in trigger area
	void OnTriggerEnter2D(Collider2D other){
		if (other.tag == "Player") {
			inZone = true;
			upgradeText.SetActive(true);
		}//if
		
		else{
			inZone = false;
		}//else
	}//OnTriggerEnter2D()
}//ActivateFireSpell
