﻿using UnityEngine;
using System.Collections;

public class ActivateTextAtLine : MonoBehaviour {

	public TextAsset theText;
	public TextBoxMonitor theTextBox;
	
	public bool destroyWhenActivated;
	public bool requireButtonPress;
	private bool buttonPress;
	public int startLine;
	public int endLine;
	
	// initialises the TextBoxMonitor component
	void Start () {
		theTextBox = FindObjectOfType<TextBoxMonitor> ();
	}//Start()
	
	//the next line of text will output when the user presses the return key
	void Update () {
		if(buttonPress && Input.GetKeyDown(KeyCode.KeypadEnter)){
			theTextBox.ReloadScript(theText);
			theTextBox.currentLine = startLine;
			theTextBox.endLine = endLine;
			theTextBox.EnableTextBox();
		}//if
	}//Update()

	//triggers and activates the dialog text at a certain line and ends at a certain line when player enters
	void OnTriggerEnter2D(Collider2D other){
		if (other.tag == "Player") {
			if(requireButtonPress){
				buttonPress = true;
				return;
			}//if

			theTextBox.ReloadScript(theText);
			theTextBox.currentLine = startLine;
			theTextBox.endLine = endLine;
			theTextBox.EnableTextBox();


		//developer can choose to destroy after being activated once
		if (destroyWhenActivated) {

			Destroy(gameObject);	
			}//if

		}//if
	}//OnTriggerEnter2D()

	//when player leaves trigger zone textbox is disabled 
	void OnTriggerExit2D(Collider2D other){
		if (other.tag == "Player") {
			buttonPress = false;		

		}//if
	}//OnTriggerExit2D
}//ActivateTextAtLines
