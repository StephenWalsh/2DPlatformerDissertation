﻿using UnityEngine;
using System.Collections;

public class TextImporter : MonoBehaviour {

	//gets text file and store each array of strings
	public TextAsset textFile;
	public string[] textLines;

	// Use this for initialization
	void Start () {
		if (textFile != null) {
			//grab the test file and seperate each string using the \n
			textLines = (textFile.text.Split('\n'));		
		}//if	
	}//Start()
}//TextImporter
