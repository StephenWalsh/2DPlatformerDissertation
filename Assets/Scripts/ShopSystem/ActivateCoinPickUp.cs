﻿using UnityEngine;
using System.Collections;

public class ActivateCoinPickUp : MonoBehaviour {

	//ActivateCoinPickUp variables
	public int upgradeCost;
	public GameObject upgradeText;
	public bool inZone = false;
	public GameObject saphirePickUp;
	public GameObject emeraldPickUp;
	public GameObject rubyPickUp;
	public GameObject diamondPickUp;
	public GameObject player;


	//these variables are not enabled when the player first plays the game
	void Start () {
		saphirePickUp.GetComponent<CoinPickup>().enabled = false;
		emeraldPickUp.GetComponent<CoinPickup>().enabled = false;
		rubyPickUp.GetComponent<CoinPickup>().enabled = false;
		diamondPickUp.GetComponent<CoinPickup>().enabled = false;
	}//Start()
	
	// if player is in zone power up can be activated
	void Update () {
		if (inZone) {
			ActivatePowerup();
		}//if
	}//Update()

	//powerup will only be activated if player is in zone and has a score greater or equal to the upgradecost
	//and subtracts the value appropriately
	void ActivatePowerup(){
		if (Input.GetKeyDown (KeyCode.Return)) {
			
			if(ScoreManager.score > upgradeCost){
				ScoreManager.AddPoints (-upgradeCost);
				saphirePickUp.GetComponent<CoinPickup>().enabled = true;
				emeraldPickUp.GetComponent<CoinPickup>().enabled = true;
				rubyPickUp.GetComponent<CoinPickup>().enabled = true;
				diamondPickUp.GetComponent<CoinPickup>().enabled = true;
				
				Destroy(gameObject);
			}//if
		}//if
	}//ActivatePowerup()

	//bool set to true if player is in trigger area
	void OnTriggerEnter2D(Collider2D other){
		if (other.tag == "Player") {
			inZone = true;
			upgradeText.SetActive(true);
		}//if

		else{
			inZone = false;
		}//else
	}//OnTriggerEnter2D()

}//ActivatecoinPickup
