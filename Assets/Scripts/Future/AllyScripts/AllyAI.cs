﻿using UnityEngine;
using System.Collections;
using Pathfinding;

[RequireComponent (typeof (Rigidbody2D))]
[RequireComponent (typeof (Seeker))]
public class AllyAI : MonoBehaviour {

	// What to chase?
	public Transform target;
	
	// How many times each second we will update our path
	public float updateRate = 2f;
	
	// Caching
	private Seeker seeker;
	private Rigidbody2D rb;
	
	//The calculated path
	public Path path;
	
	//The AI's speed per second
	public float speed = 300f;
	public ForceMode2D fMode;
	
	[HideInInspector]
	public bool pathIsEnded = false;
	
	// The max distance from the AI to a waypoint for it to continue to the next waypoint
	public float nextWaypointDistance = 3;
	
	// The waypoint we are currently moving towards
	private int currentWaypoint = 0;
	
	private bool searchingForPlayer = false;
	
	
	
	private Transform enemyGraphics;
	private bool facingRight = false; // For determining which way the player is currently facing.
	
	
	void Start () {
		seeker = GetComponent<Seeker>();
		rb = GetComponent<Rigidbody2D>();
		enemyGraphics = transform.FindChild("EnemyGraphics");
		
		
		
		if (target == null) {
			
			if (!searchingForPlayer){
				searchingForPlayer = true;
				StartCoroutine (SearchForPlayer());
			}
			return;
		}
		
		// Start a new path to the target position, return the result to the OnPathComplete method
		seeker.StartPath (transform.position, target.position, OnPathComplete);
		
		StartCoroutine (UpdatePath ());
		
	}
	
	
	IEnumerator SearchForPlayer(){
		GameObject sResult = GameObject.FindGameObjectWithTag ("enemy");
		//We can't access the .transform parameter if the search doesn't find anything that's why we  can't just:
		//target = GameObject.FindGameObjectWithTag ("Player").transform;
		//first store in a variable, then check if that variable is found (==null)or not
		if (sResult == null) {
			yield return new WaitForSeconds (0.5f);	
			StartCoroutine (SearchForPlayer ());
		}//if 
		else {
			target = sResult.transform;
			searchingForPlayer = false;
			
			//when you return out of a method it's going to stop going.  So updatepath will stop being called. So to fix this
			StartCoroutine(UpdatePath());
			return false;
		}
	}//SearchForPlayer
	
	
	IEnumerator UpdatePath () {
		if (target == null) {
			
			if (!searchingForPlayer){
				searchingForPlayer = true;
				StartCoroutine (SearchForPlayer());
			}
			return false;
		}
		
		// Start a new path to the target position, return the result to the OnPathComplete method
		seeker.StartPath (transform.position, target.position, OnPathComplete);
		
		yield return new WaitForSeconds ( 1f/updateRate );
		StartCoroutine (UpdatePath());
	}
	
	public void OnPathComplete (Path p) {
		Debug.Log ("We got a path. Did it have an error? " + p.error);
		if (!p.error) {
			path = p;
			currentWaypoint = 0;
		}
	}
	
	void FixedUpdate () {
		if (target == null) {
			
			if (!searchingForPlayer){
				searchingForPlayer = true;
				StartCoroutine (SearchForPlayer());
			}
			return;
		}
		
		//TODO: Always look at player?
		
		if (path == null)
			return;
		
		if (currentWaypoint >= path.vectorPath.Count) {
			if (pathIsEnded)
				return;
			
			Debug.Log ("End of path reached.");
			pathIsEnded = true;
			return;
		}
		pathIsEnded = false;
		
		//Direction to the next waypoint
		Vector3 dir = ( path.vectorPath[currentWaypoint] - transform.position ).normalized;
		dir *= speed * Time.fixedDeltaTime;
		
		//Move the AI
		rb.AddForce (dir, fMode);
		
		float dist = Vector3.Distance (transform.position, path.vectorPath[currentWaypoint]);
		if (dist < nextWaypointDistance) {
			currentWaypoint++;
			return;
		}
		//If the input is moving the player right and the player is facing left...
		if (dir.x > 0 && !facingRight)
			// ... flip the player.
			Flip ();
		// Otherwise if the input is moving the player left and the player is facing right...
		else if (dir.x < 0 && facingRight)
			// ... flip the player.
			Flip ();
	}
	
	
	private void Flip()
	{
		// Switch the way the player is labelled as facing.
		facingRight = !facingRight;
		
		// Multiply the player's x local scale by -1.
		Vector3 theScale = enemyGraphics.localScale;
		theScale.x *= -1;
		enemyGraphics.localScale = theScale;
	}

}
