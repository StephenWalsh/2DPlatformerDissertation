﻿using UnityEngine;
using System.Collections;

public class AllyShootAtEnemy : MonoBehaviour {

	
	public float playerInRange;
	public GameObject enemyStar;
	//public GameObject playerxxx;
	public EnemyFly player;
	public Transform launchPoint;
	public float waitBetweenShots;
	private float shotCounter;
	
	public LayerMask whatToHit;
	
	
	// Use this for initialization
	void Start () {
		//		playerxxx = GameObject.FindGameObjectWithTag("Player(Clone)");
		
		//player = GameObject.FindGameObjectWithTag ("enemy");
		
		shotCounter = waitBetweenShots;
	}
	
	// Update is called once per frame
	void Update () {
		player = FindObjectOfType<EnemyFly>();

		Debug.DrawLine (new Vector3(transform.position.x - playerInRange, transform.position.y, transform.position.z),
		                new Vector3(transform.position.x + playerInRange, transform.position.y, transform.position.z));
		
		//only if shot counter is 
		shotCounter -= Time.deltaTime;
		
		if (player.transform.position.x > transform.position.x && 
		    player.transform.position.x < transform.position.x + playerInRange && shotCounter<0) {
			Instantiate(enemyStar, launchPoint.position, launchPoint.rotation);
			shotCounter = waitBetweenShots;
		}
		
		if (transform.localScale.x > 0 && player.transform.position.x < transform.position.x && 
		    player.transform.position.x > transform.position.x - playerInRange && shotCounter<0) {
			Instantiate(enemyStar, launchPoint.position, launchPoint.rotation);
			shotCounter = waitBetweenShots;
			
		}
		
		
		
	}
}
