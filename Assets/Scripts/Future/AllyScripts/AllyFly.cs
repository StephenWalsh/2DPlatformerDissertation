﻿using UnityEngine;
using System.Collections;

public class AllyFly : MonoBehaviour {

	public EnemyFlyStats enemyFlyStats = new EnemyFlyStats();

	
	[System.Serializable]
	public class EnemyFlyStats{
		public int maxHealth = 40;
		
		private int _curHealth;
		public int curHealth {
			get {return _curHealth;}
			set {_curHealth = Mathf.Clamp(value, 0, maxHealth);}
		}
		
		public void Init(){
			curHealth = maxHealth;
		}
	}
	public int Damage = 10;
	
	[Header("Optional: ")]
	[SerializeField]
	private StatusIndicator statusIndicator;
	
	
	void Start(){
		enemyFlyStats.Init ();
		
		if (statusIndicator != null) {
			statusIndicator.SetHealth(enemyFlyStats.curHealth, enemyFlyStats.maxHealth);
		}
	}
	
	public void DamageEnemyFly(int damage){
		
		enemyFlyStats.curHealth -= damage;
		if (enemyFlyStats.curHealth <= 0) {
			//GameMaster.KillEnemy(this);
			Destroy(gameObject);
			
			//			GameMaster.Instantiate();
		}//if
		if (statusIndicator != null) {
			statusIndicator.SetHealth(enemyFlyStats.curHealth, enemyFlyStats.maxHealth);
		}
	}//DamagePlayer() with one argument
	
	EnemyFly player;
	
	void OnCollisionEnter2D(Collision2D col){
		if(col.gameObject.tag == "enemy"){
			Debug.Log("Ally did" +  Damage + " Damage.");
			player.DamageEnemyFly(Damage);
			//player.transform.position.x -= 2;
		}
		
	}
}
