﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class CombatText : MonoBehaviour {

	private static CombatText instance;

	public GameObject textPrefab;

	public RectTransform canvasTransform;

	public static CombatText Instance{
		get{
			if(instance ==null){
				instance = GameObject.FindObjectOfType<CombatText>();
			}
			return instance;
		}
	}

	public void CreateText(Vector3 position){
		GameObject scrollingCombatText = (GameObject) Instantiate (textPrefab,position, Quaternion.identity);
	
		scrollingCombatText.transform.SetParent (canvasTransform);
		scrollingCombatText.GetComponent<RectTransform> ().localScale = new Vector3(1,1,1);
	}

}
