﻿using UnityEngine;
using System.Collections;

public class PlayerForwardMelee : MonoBehaviour {


	public float speed;
	public int damageToGive;

	public PlayerX player;
	public GameObject impactEffect;
	public GameObject instantiatedObj;
	public float rotationSpeed;

	public Transform firePoint;
	public Transform groundPoint;
	public GameObject basicSpell;

	// Use this for initialization
	void Start () {
		player = FindObjectOfType<PlayerX> ();
	
		if (player.transform.localScale.x < 0) {
			speed = -speed;		
			rotationSpeed = -rotationSpeed;

		}

	}
	
	// Update is called once per frame
	void Update () {
		GetComponent<Rigidbody2D> ().velocity = new Vector2 (speed, GetComponent<Rigidbody2D> ().velocity.y);
		GetComponent<Rigidbody2D> ().angularVelocity = rotationSpeed;
		if (Input.GetKeyDown (KeyCode.Mouse1)) {
			Shoot();
		}


	}

	void Shoot(){
		instantiatedObj = (GameObject) Instantiate(basicSpell, firePoint.position, firePoint.rotation);
			
			
	}

	void OnTriggerEnter2D(Collider2D other){
		EnemyFly enemy = other.GetComponent<EnemyFly> ();
		
		if(enemy != null){
			enemy.DamageEnemyFly(damageToGive);
			Instantiate (impactEffect, other.transform.position, other.transform.rotation);
			Destroy (basicSpell);		
		}	
		if(other.tag== "Ground"){
			Instantiate (impactEffect, other.transform.position, other.transform.rotation);
			//Destroy (instantiatedObj);		
			Destroy (basicSpell);		

		}	
		if (enemy = null) {
			return;		
		}
	}
}
