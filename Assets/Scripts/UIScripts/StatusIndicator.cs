﻿using UnityEngine;
using UnityEngine.UI;

public class StatusIndicator : MonoBehaviour {

	//Status Indicator variables 
	[SerializeField]
	private RectTransform healthbarRect;

	//Just report an error if no healthbar referenced
	void Start () {
		if (healthbarRect == null) {
			Debug.LogError("STATUS INDICATOR: No Healthbar object referenced!");		
		}//if
	}//Start()
	
	//set the green rectangle transform in respect to the gameobject's current health
	public void SetHealth(int _cur, int _max){
		float _value = (float) _cur / _max;
		healthbarRect.localScale = new Vector3 (_value, healthbarRect.localScale.y, healthbarRect.localScale.z);
	}//SetHealth()
}//StatusIndicator
