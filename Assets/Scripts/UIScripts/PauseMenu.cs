﻿using UnityEngine;
using System.Collections;

public class PauseMenu : MonoBehaviour {



	public string levelSelect;
	public string mainMenu;
	public bool isPaused;
	public GameObject pauseMenuDisplay;
	public GameObject instructionsUI;
	public bool isInstructions;

	void Awake(){
		PlayerPrefs.SetString("CurrentLevel", Application.loadedLevelName);

	}
	
	// Update is called once per frame
	void Update () {
		//if paused, display pause screen stop time completely
		if (isInstructions) {
			
			instructionsUI.SetActive (true);	
			Time.timeScale = 0f;
			
		}//if
		else{
			instructionsUI.SetActive (false);	
			Time.timeScale = 1f;
		}//else
		
		if(Input.GetKeyDown(KeyCode.I)){
			isInstructions = !isInstructions;
		}//if



		//if paused, display pause screen stop time completely
		if (isPaused) {

			pauseMenuDisplay.SetActive(true);
			Time.timeScale = 0f;

		}//if
		else{
			pauseMenuDisplay.SetActive(false);
			Time.timeScale = 1f;
		}//else

		if(Input.GetKeyDown(KeyCode.P)){
			isPaused = !isPaused;
		}//if
	}//Update()


	public void Resume(){
		isPaused = false;
	}

	public void LevelSelect(){
		PlayerPrefs.SetString ("CurrentLevel", Application.loadedLevelName);
	//	Application.LoadLevel (levelSelect);

	}

	public void Instructions(){
		instructionsUI.SetActive (true);	
		if(Input.GetKeyDown(KeyCode.Return)){
			instructionsUI.SetActive (false);	
		}//if
	}//Instructions()

	public void Quit(){
		Application.LoadLevel (mainMenu);

	}
}
