﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class DialogueScript : MonoBehaviour {

	//DialogueScript variables
	public GameObject note;
	public GameObject panel;

	//makes sure signs are not active when game start
	void Start () {
		
		note.SetActive (false);
		panel.SetActive (false);
		
	}//Start()

	//if player enters the trigger zone activate sign message
	void OnTriggerEnter2D(Collider2D collision){
		if (collision.gameObject.tag=="Player"){
			note.SetActive (true);
			panel.SetActive (true);
		}//if
	}//OnTriggerEnter2D

	//if player leaves the trigger zone de-activate sign message
	void OnTriggerExit2D(Collider2D collision){
		if (collision.gameObject.tag=="Player"){
			note.SetActive (false);
			panel.SetActive (false);
		}//if

	}//OnTriggerExit2D

}//DialogueScript