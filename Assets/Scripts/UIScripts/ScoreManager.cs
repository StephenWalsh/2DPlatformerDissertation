﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
public class ScoreManager : MonoBehaviour {

	//we want access for these objects throughout all scripts, as score must carry between all scenes (static variable)
	public static int score;
	Text text;

	void Start(){
		text = GetComponent<Text> ();
		score = PlayerPrefs.GetInt ("PlayerScore");
	}//Start()

	//display score UI text 
	void Update(){
		//don't want the player having minus points
		if (score < 0) {
			score = 0;
		}//if
			text.text = "" + score;
	}//Update()

	//adds the appropriate points and saves them via PlayerPrefs function
	public static void AddPoints(int pointsToAdd){
		score += pointsToAdd;
		PlayerPrefs.SetInt ("PlayerScore", score);
	}//AddPoints()

	//When user slects a new game, the score is reset
	public static void Reset(){
		score = 0;
		PlayerPrefs.SetInt ("PlayerScore", score);
	}//Reset()

}//ScoreManager
