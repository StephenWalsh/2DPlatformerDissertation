﻿using UnityEngine;
using System.Collections;

public class NextLevelScript2 : MonoBehaviour {


	public string levelTag;

	void OnTriggerEnter2D(Collider2D other)
	{
		if (other.tag == "Player")
			PlayerPrefs.SetInt(levelTag, 1);
			Application.LoadLevel("LevelSelectScreen");
	}
}
