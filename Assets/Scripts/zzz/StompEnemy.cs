﻿using UnityEngine;
using System.Collections;

public class StompEnemy : MonoBehaviour {


	public int damageToGive;
	public float bounceOnHead;
	private PlayerX myRigidBody2D;
	// Use this for initialization
	void Start () {
	
		myRigidBody2D = GetComponent<PlayerX>();
	}
	
	// Update is called once per frame
	void Update () {
	}
	void OnTriggerEnter2D(Collider2D other){

		EnemyFly enemy = other.GetComponent<EnemyFly> ();

		if(enemy != null){
			enemy.DamageEnemyFly(damageToGive);
			myRigidBody2D.transform.position = new Vector2( 0f, 10f) ;
		}
	}
}
