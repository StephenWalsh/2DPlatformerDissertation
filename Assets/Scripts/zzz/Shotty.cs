﻿using UnityEngine;
using System.Collections;

public class Shotty : MonoBehaviour {

	public string[] levelTags;
	//visually unlock
	public GameObject[] locks;
	//functionally unlock each level
	public bool[] levelUnlocked;

	void Start(){

		for (int i = 0; i < levelTags.Length; i++) {

			if(PlayerPrefs.GetInt(levelTags[i]) != null){
				levelUnlocked[i] = false;
			}		
			else if(PlayerPrefs.GetInt(levelTags[i]) == 0){
				levelUnlocked[i] = false;
			}
			else {
				levelUnlocked[i] = true;
			}
		
			if(levelUnlocked[i]){
				locks[i].SetActive(true);
			}
		}


	}
	
}
