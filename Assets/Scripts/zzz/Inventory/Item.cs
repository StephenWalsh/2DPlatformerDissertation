﻿using UnityEngine;
using System.Collections;

//creates your own datatype for 2 items
public enum ItemType{MANA, HEALTH}

public class Item : MonoBehaviour {


	public ItemType type;
	//every type of item needs a sprite
	public Sprite spriteNeutral;
	public Sprite spriteHighlighted;
	//needs items to stacck items not able to stack

	public int MaxStackSize;


	public void UseItem(){

		switch (type){
		case ItemType.MANA:
			Debug.Log("Used Mana Potion");
			break;
		case ItemType.HEALTH:
			Debug.Log("Used Health Potion");
			break;			
		}
		}
}
