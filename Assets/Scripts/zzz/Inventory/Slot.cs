﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
public class Slot : MonoBehaviour {


	//The type of objects wthe stack will contain: Stack<item>
	//the Stack's identifier: items
	private Stack<Item> items = new Stack<Item>();
	public Text stackText;


	public Sprite slotEmpty;
	public Sprite slotHighlight;

	public bool isEmpty {
		get{ return items.Count == 0;}
	}

	//Pushing these objects we're adding/pushing into the stack. Item will be pushed to the bottom of stack |_|
	//items.Push(potion);

	//This pops/takes the first item from the stack. same as java
	//items.Pop(potion)

	//look at an item through your stack without touching it:
	//items.Peek();

	// Use this for initialization
	void Start () 
	{   
		//Instantiates the items stack
		items = new Stack<Item>();
		
		//Creates a reference to the slot slot's recttransform
		RectTransform slotRect = GetComponent<RectTransform>();
		
		//Creates a reference to the stackTxt's recttransform
		RectTransform txtRect = stackText.GetComponent<RectTransform>();
		
		//Calculates the scalefactor of the text by taking 60% of the slots width
		int txtScleFactor = (int)(slotRect.sizeDelta.x * 0.60);
		
		//Sets the min and max textSize of the stackTxt
		stackText.resizeTextMaxSize = txtScleFactor;
		stackText.resizeTextMinSize = txtScleFactor;
		
		//Sets the actual size of the txtRect
		txtRect.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, slotRect.sizeDelta.x);
		txtRect.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, slotRect.sizeDelta.y);
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void AddItem(Item item){

		items.Push (item);

		if (items.Count > 1) {
			stackText.text = items.Count.ToString();

		}
	
		ChangeSprite (item.spriteNeutral, item.spriteHighlighted);
	}

	private void ChangeSprite(Sprite neutral, Sprite highlighted){
		GetComponent<Image> ().sprite = neutral;

		SpriteState st = new SpriteState ();

		st.highlightedSprite = highlighted;
		st.pressedSprite = neutral;

	
		//add these to the button states.  This changes the state of the button!
		GetComponent<Button> ().spriteState = st;
	
	
	}

}
