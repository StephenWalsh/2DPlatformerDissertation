﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Inventory : MonoBehaviour {

	private RectTransform inventoryRect;
	
	private float inventoryWidth, inventoryHeight;
	public int slots;
	public int rows;
	public float slotPaddingLeft, slotPaddingTop;
	public float slotSize;
	public GameObject slotPrefab;
	//this inventory is going to hold all the list of objects we have
	private List<GameObject> allSlots;

	//create our inventory using the variables


	private int emptySlot;
	// Use this for initialization
	void Start () {
	
		CreateLayout ();
	}
	
	// Update is called once per frame
	void Update () {
	
	}


	/// <summary>
	/// Creates the inventory's layout
	/// </summary>
	private void CreateLayout()
	{
		//Instantiates the allSlot's list
		allSlots = new List<GameObject>();
		
		emptySlot = slots;
		//Calculates the width of the inventory
		inventoryWidth = (slots / rows) * (slotSize + slotPaddingLeft) + slotPaddingLeft;
		
		//Calculates the highs of the inventory
		inventoryHeight = rows * (slotSize + slotPaddingTop) + slotPaddingTop;
		
		//Creates a reference to the inventory's RectTransform
		inventoryRect = GetComponent<RectTransform>();
		
		//Sets the with and height of the inventory.
		inventoryRect.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, inventoryWidth);
		inventoryRect.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, inventoryHeight);
		
		//Calculates the amount of columns
		int columns = slots / rows;
		
		for (int y = 0; y < rows; y++) //Runs through the rows
		{
			for (int x = 0; x < columns; x++) //Runs through the columns
			{   
				//Instantiates the slot and creates a reference to it
				GameObject newSlot = (GameObject)Instantiate(slotPrefab);
				
				//Makes a reference to the rect transform
				RectTransform slotRect = newSlot.GetComponent<RectTransform>();
				
				//Sets the slots name
				newSlot.name = "Slot";
				
				//Sets the canvas as the parent of the slots, so that it will be visible on the screen
				newSlot.transform.SetParent(this.transform.parent);
				
				//Sets the slots position
				slotRect.localPosition = inventoryRect.localPosition + new Vector3(slotPaddingLeft * (x + 1) + (slotSize * x), -slotPaddingTop * (y + 1) - (slotSize * y));
				
				//Sets the size of the slot
				slotRect.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, slotSize);
				slotRect.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, slotSize);
				
				//Adds the new slots to the slot list
				allSlots.Add(newSlot);
				
			}
		}
	}

	/// <summary>
	/// Adds an item to the inventory
	/// </summary>
	/// <param name="item">The item to add</param>
	/// <returns></returns>
	public bool AddItem(Item item)
	{
		if (item.MaxStackSize == 1) //If the item isn't stackable
		{   
			//Places the item at an empty slot
			PlaceEmpty(item);
			return true;
		}
		else //If the item is stackable 
		{
			foreach (GameObject slot in allSlots) //Runs through all slots in the inventory
			{
				Slot tmp = slot.GetComponent<Slot>(); //Creates a reference to the slot
				
				if (!tmp.isEmpty) //If the item isn't empty
				{
					//Checks if the om the slot is the same type as the item we want to pick up
					//if (tmp.CurrentItem.type == item.type && tmp.IsAvailable) 
					//{
				//		tmp.AddItem(item); //Adds the item to the inventory
			//			return true;
		//			}
				}
			}
			if (emptySlot > 0) //Places the item on an empty slots
			{
				PlaceEmpty(item);
			}
		}
		
		return false;
	}

	/// <summary>
	/// Places an item on an empty slot
	/// </summary>
	/// <param name="item"></param>
	/// <returns></returns>
	private bool PlaceEmpty(Item item)
	{
		if (emptySlot > 0) //If we have atleast 1 empty slot
		{
			foreach (GameObject slot in allSlots) //Runs through all slots
			{
				Slot tmp = slot.GetComponent<Slot>(); //Creates a reference to the slot 
				
				if (tmp.isEmpty) //If the slot is empty
				{
					tmp.AddItem(item); //Adds the item
					emptySlot--; //Reduces the number of empty slots
					return true;
				}
			}
		}
		
		return false;
	}




}











	