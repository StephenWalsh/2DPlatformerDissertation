﻿using UnityEngine;
using System.Collections;

public class Parallaxing : MonoBehaviour {


	//declare variables
	//create an array for elements we need to apply parallaxing too (background and foreground)

	public Transform[] backgrounds;			//array of all the back and foreground to be parallaxed
	private float[] parallaxScales;			//The proportion of the cameras movement to move the backgrounds by
	public float smoothing = 1f;				//parallaxing amount: how smooth the parallax is going to be/  Make sure >0

	private Transform cam;					//reference to the main cameras transform
	private Vector3 previousCamPos;		 	//store position of camera in the previous frame

	//Awake() is called before start, it will call all logic before the start function before the camera is set up - great for references
	void Awake() {
	//set up the camera reference
	cam = Camera.main.transform;
	}


	// Use this for initialization
	void Start () {
		//the previous frame had the current frame's camera position
		previousCamPos = cam.position;

		//making sure the parallaxing scales array is just as long as the background
		parallaxScales = new float[backgrounds.Length];

		//then going through each background and assigning the background said pos into the corresponding parallaxing scale
		//assign corresponding parallax scales
		for (int i = 0; i < backgrounds.Length; i++) {
			parallaxScales[i] = backgrounds[i].position.z * -1;		
		}
	}
	
	// Update is called once per frame
	void Update () {
	//for each background

		for (int i = 0; i < backgrounds.Length; i++) {
			//parallaxing is the oppositew of the camera movement, because the previous frame multiplied by  the scale
			//saying that the parallax should be the difference between what the camera position is now and what it was before
			//hw much it's moved and multiply by the amount of parallaxing we want it to be 
			float parallax = (previousCamPos.x - cam.position.x) * parallaxScales[i];

			//set a target x position which is the current position plus the parallax
			//in this we are storing the background position x plus the parallaxing
			float backgroundTargetPosX = backgrounds[i].position.x + parallax;

			//Create a target position which is the backgrounds current pos with its target x position
			//assigns background target position on the x and take the same y and z coordinates as it did before
			//could do this on y coordinate as well - however as we're not going to be moving that much up or down leave it out atm ****
			Vector3 backgroundTargetPos = new Vector3 (backgroundTargetPosX, backgrounds[i].position.y, backgrounds[i].position.z);


			//fade between current position and the target postion using what's called Lerp.  
			//Lerp is a built in function that fades inbetween positions
			//Time.deltaTime converts frames to seconds
			backgrounds[i].position = Vector3.Lerp (backgrounds[i].position, backgroundTargetPos, smoothing * Time.deltaTime);
	
		}

			//Set the previous cam pos to the cameras pos at the end of the frame
			previousCamPos = cam.position;
	}
}
