﻿using UnityEngine;
using System.Collections;

public class ArmRotation : MonoBehaviour {


	//unity has a built function for following mouse cursor - transform.lookat

	public int rotationOffset = 90;
	
	// Update is called once per frame
	void Update () {
	
				Vector3 difference = Camera.main.ScreenToWorldPoint (Input.mousePosition) - transform.position;  //subtract position player position from the mouse position
				difference.Normalize (); // normalising vector: 
				// we keep the same proportion but were making them all smaller so when added together equal 1
	
		float rotZ = Mathf.Atan2 (difference.y, difference.x) * Mathf.Rad2Deg;	//find angle and convert from radians to degress

		transform.rotation = Quaternion.Euler (0f, 0f, rotZ + rotationOffset);
	}
		
	}

