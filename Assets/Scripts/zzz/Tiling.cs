﻿using UnityEngine;
using System.Collections;

//check if component is attached to the game object and if not create one
//do this by using sprite renderers 
[RequireComponent(typeof(SpriteRenderer))]

public class Tiling : MonoBehaviour {
	//want to offset because it's prone to errors, set b is 2 to the right
	public int offsetX = 5; //offset so that we don't get any graphical errors
	public bool hasARightBuddy = false;//both used to check if we need to insert stuff
	public bool hasALeftBuddy = false;
	
	//does it already have a buddy?  insert one
	
	public bool reverseScale = false; //used if the object is not tilable
	
	private float spriteWidth = 0f; //the width of our element
	private Camera cam;
	private Transform myTransform;  
	
	void Awake() { //does all the referencign between scripts
		cam = Camera.main;
		myTransform = transform;
	}
	
	// Use this for initialization
	void Start () {
		SpriteRenderer sRenderer = GetComponent<SpriteRenderer>(); //gets the compnent type we want to get
		spriteWidth = sRenderer.sprite.bounds.size.x; //this will give us the width of the sprite no mateer how we size it
	}
	
	// Update is called once per frame
	void Update () {
		//does it still need buddys - if not do nothing - update if it needs it
		if (hasALeftBuddy == false || hasARightBuddy == false){
			//this will calculate the length of the camera extend (i.e. half the width) from centre to the right bar
			float camHorizontalExtend = cam.orthographicSize * (Screen.width/Screen.height); 
			
			//now calculate the x position where the camera can see the edge of the sprite
			//calculate the x position where the camera can see the edge of the sprite (element)
			float edgeVisiblePositionRight = (myTransform.position.x + spriteWidth/2) - camHorizontalExtend;
			float edgeVisiblePositionLeft = (myTransform.position.x - spriteWidth/2) + camHorizontalExtend;
			
			//checking if b is greater or equal than a minus our offset so it doesn't 
			//checking if we can see the edge of the element and then calling MakeNewBuddy if we can.
			if(cam.transform.position.x >= edgeVisiblePositionRight - offsetX && hasARightBuddy == false){
				MakeNewBuddy (1);
				hasARightBuddy = true;
				
			}
			else if (cam.transform.position.x <= edgeVisiblePositionLeft + offsetX && hasALeftBuddy == false){
				MakeNewBuddy (-1);
				hasALeftBuddy = true;
			}
		}
	}
	
	
	//this inverts the right or left buddy background sprites
	//A function/method that creates a buddy on the side required
	void MakeNewBuddy (int rightOrLeft) {
		//calculating the new position of our new buddy
		Vector3 newPosition = new Vector3(myTransform.position.x + spriteWidth * rightOrLeft, myTransform.position.y, myTransform.position.z);
		//actual spawning of the newBuddy object - 3 parameters:
		//1. transform with want you want to instantiate 2.storing the position 3. stores rotation
		Transform newBuddy = Instantiate(myTransform, newPosition, myTransform.rotation) as Transform;
		//need to cast this Transform just verifies the type the newBuddy object is going to be - we need to do the same for the instantiate
		//as Transform or (Transform) at start
		
		//if not tilable lets reverse the x size of objects to get rid of offputting seems
		if (reverseScale == true){
			newBuddy.localScale = new Vector3 (newBuddy.localScale.x*-1, newBuddy.localScale.y, newBuddy.localScale.z);
		}
		
		newBuddy.parent = myTransform.parent;
		if (rightOrLeft > 0f) {
			newBuddy.GetComponent<Tiling>().hasALeftBuddy = true;
		}
		else {
			newBuddy.GetComponent<Tiling>().hasARightBuddy = true;
			
		}
	}
}