﻿using UnityEngine;
using System.Collections;

public class BasicAttack : MonoBehaviour {

	GameObject player;
	public int attackDamage = 10;
	public float timeBetweenAttacks = 0.5f;
	Animator anim;
	Player playerHealth;
	bool playerInRange;
	float timer;

	void Awake(){
		player = GameObject.FindGameObjectWithTag ("Player");
		playerHealth = player.GetComponent<Player>();
		anim = GetComponent <Animator>();
	}

	void OnTriggerEnter(Collider other){
			if (other.gameObject == player) {
			playerInRange = true;		
		}
	}

	void OnTriggerExit(Collider other){
		if (other.gameObject == player) {
			playerInRange = false;		
		}
	}

	// Update is called once per frame
	void Update () {
		timer += Time.deltaTime;

		if(timer >= timeBetweenAttacks && playerInRange){
			Attack();
		}
		if (playerHealth.currentHealth <= 0) {

		}

	}


	void Attack(){
		timer = 0f;

		if (playerHealth.currentHealth > 0) {
			playerHealth.DamagePlayer(attackDamage);		
		}
	}




}
