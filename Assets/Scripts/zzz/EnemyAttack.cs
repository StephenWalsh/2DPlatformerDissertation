﻿using UnityEngine;
using System.Collections;

public class EnemyAttack : MonoBehaviour {
	
	public Transform player;
	
	public float range = 2;
	private bool onRange= false;
	public float timeBetweenAttacks = 0.5f;
	Animator anim;
	GameObject playerX;
	Player playerHealth;
	bool playerInRange;
	float timer;

	
	
	//want the player to hold down the mouse button to fire in intervals
	//but also want there to be intervals between each fire. i.e. rate of fire
	public float fireRate = 0;
	public int Damage = 10;
	//tell us what we want to hit.  Not to hit the player but everything else
	public LayerMask whatToHit;
	
	//private variables
	float timeToFire = 0;
	Transform firePoint;
	
	
	//Bullet trial (BT)
	public Transform BulletTrialPrefab;
	public Transform MuzzleFlashPrefab;
	
	float timeToSpawnEffect = 0;
	float effectSpawnRate = 1;
	
	
	
	
	void Awake(){
		
		//searches for the children of Lantern rather than labelling it by its specified name in Unity
		firePoint = transform.FindChild ("FirePoint");
		
		//null check (good practice) a good error to know what's wrong
		if (firePoint == null) {
			Debug.LogError("no fire point!");
		}
		

	}
	
	void Shoot(){
		
		//creating new vector2 assigning it to world point for both x and y axis. Translates position 
		//to the mouse to screen coordinates to position in the world and use to create a raycast 

		if (player == null) {
			StartCoroutine("MyMethod");
		} else {
						Vector2 mousePosition = new Vector2 (player.position.x,
		                                     player.position.y);
				
						//take firepoint storing that position as a 2d axis i.,e. vector2
						Vector2 firePointPosition = new Vector2 (firePoint.position.x, firePoint.position.y);
		
						//Raycast won't take 2 arguments.  It takes the origin, "the direction and distance=100" and also give it a layer mask
						RaycastHit2D hit = Physics2D.Raycast (firePointPosition, mousePosition-firePointPosition, 20, whatToHit);
		
			Debug.DrawLine (firePointPosition, mousePosition, Color.yellow);
				
		if (hit.collider != null) {

			Debug.DrawLine(firePointPosition, hit.point, Color.red);
			Player xxx = hit.collider.GetComponent<Player>();
			if (xxx != null){
			Debug.Log("Enemy " + hit.collider.name + " and did " + Damage + " Damage.");
			xxx.DamagePlayer(Damage);
					StartCoroutine("MyMethod");
					Effect();
					GetComponent<AudioSource>().Play();
				}
			}
		
		}
	}

	IEnumerator MyMethod() {
		Debug.Log("Before Waiting 2 seconds");
		yield return new WaitForSeconds(3);
		Debug.Log("After Waiting 2 Seconds");
	}
	
	void Update() {

		if (player == null) {
			StartCoroutine("MyMethod");

			GameMaster.gm.RespawnPlayer();
			StartCoroutine("MyMethod");					} 
		else {
			onRange = Vector3.Distance (transform.position, player.position) < range;

			if (onRange) {

			transform.LookAt (player);
			Shoot ();
			Effect();
			StartCoroutine("MyMethod");


						}
			else {
				//GameMaster.gm.RespawnPlayer(player);
				StartCoroutine("MyMethod");
			} 
		}
	}
	
	
	
	void Effect(){
		//need to show v prefab 
		//instantiate is an instance, this instance to store in a variable to 
		//effect one instance of a prefrab and not all of them
		Transform clone = Instantiate (BulletTrialPrefab, transform.position, player.rotation) as Transform;
		//create a cast to make it such that it's a transform
		//make sure it's parented to firepoint
		clone.parent = firePoint;
		float size = Random.Range (0.6f, 0.9f);
		clone.localScale = new Vector3 (size, size, size);
		Destroy (clone.gameObject, 0.2f);
	}
	
	
	
}
