﻿using UnityEngine;
using System.Collections;

public class EnemyFly : MonoBehaviour {

	//Enemy Fly variables
	public EnemyFlyStats enemyFlyStats = new EnemyFlyStats();
	public GameObject GemSaphire ;
	public GameObject GemEmerald;
	public GameObject GemRuby ;
	public GameObject GemDiamond ;
	public GameObject heart ;
	Vector3 coinMove;
	Player player;


	[System.Serializable]
	public class EnemyFlyStats{
		public int maxHealth = 40;

		private int _curHealth;
		public int curHealth {
			get {return _curHealth;}
			set {_curHealth = Mathf.Clamp(value, 0, maxHealth);}
		}//curHealth

		public void Init(){
			curHealth = maxHealth;
		}//Init()
	}//EnemyFlystats
	public int Damage = 10;

	//optional variables
	[Header("Optional: ")]
	[SerializeField]
	private StatusIndicator statusIndicator;
	private Animator anim;
	public Transform effect;
	public GameObject aOrb;

	//activates status indicator for all enemies
	void Awake(){
		enemyFlyStats.Init ();

		if (statusIndicator != null) {
			statusIndicator.SetHealth(enemyFlyStats.curHealth, enemyFlyStats.maxHealth);
		}//if
		if (statusIndicator == null) {
			return;		
		}//if
	}//Awake()

	//Damages enemie fly from the weapons script
	//Randomises gold drops when enemy is destroyed
	public void DamageEnemyFly(int damage){

		enemyFlyStats.curHealth -= damage;
		if (enemyFlyStats.curHealth <= 0) {
			Instantiate (effect, transform.position, transform.rotation);
			GameMaster.KillEnemy(this);
			//Instantiate gold
			for(int i = 0; i<Random.Range(0,4); i++){
				Instantiate(GemSaphire, (transform.position + new Vector3(Random.Range(-3.0f,3.0f),Random.Range(0f,3.0f),0f)*
				                         Random.Range(1.0f, 1.0f)), Quaternion.identity);			
			}//for
			for(int i = 0; i<Random.Range(0,3); i++){
				Instantiate(GemEmerald, (transform.position + new Vector3(Random.Range(-3.0f,3.0f),Random.Range(0f,3.0f),0f)*
				                         Random.Range(1.0f, 1.0f)), Quaternion.identity);			
			}//for
			for(int i = 0; i<Random.Range(0,2); i++){
				Instantiate(GemRuby, (transform.position + new Vector3(Random.Range(-3.0f,3.0f),Random.Range(0f,3.0f),0f)*
				                      Random.Range(1.0f, 1.0f)), Quaternion.identity);			
			}//for
			for(int i = 0; i<Random.Range(0,1); i++){
				Instantiate(GemDiamond, (transform.position + new Vector3(Random.Range(-3.0f,3.0f),Random.Range(0f,3.0f),0f)*
				                         Random.Range(1.0f, 1.0f)), Quaternion.identity);			
			}//for
			for(int i = 0; i<Random.Range(0,1); i++){
				Instantiate(heart, (transform.position + new Vector3(Random.Range(-3.0f,3.0f),Random.Range(0f,3.0f),0f)*
				                    Random.Range(1.0f, 1.0f)), Quaternion.identity);			
			}//for
		}//if
		if (aOrb != null) {
			aOrb.SetActive (true);
		}//if

		if (statusIndicator != null) {
			statusIndicator.SetHealth(enemyFlyStats.curHealth, enemyFlyStats.maxHealth);
		}//if
	}//DamagePlayer() with one argument


	void OnCollisionEnter2D(Collision2D col){
		if(col.gameObject.name == "Player"){
			Debug.Log("Enemy " +  Damage + " Damage.");
			player.DamagePlayer(Damage);
		}//if
	}//OnCollisionEnter2D()



}//EnemyFly
