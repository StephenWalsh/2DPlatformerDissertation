﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Player : MonoBehaviour {

	//Player variables
	[System.Serializable]
	public class PlayerStats{

	}//PlayerStats
	public int maxHealth = 100;
	public int currentHealth;
	public int maxMana = 100;
	public int currentMana;
	public Slider healthSlider;
	public Slider manaSlider;
	public Image damageImage;
	public AudioClip deathClip;
	public float flashSpeed = 5f;
	public Color flashColour = new Color (1f, 0f, 0f, 0.1f);

	Animator anim;
	AudioSource playerAudio;
	private LevelManager levelManager;
	
	bool isDead;
	bool damaged = false;
	public PlayerStats playerStats = new PlayerStats();
	public int fallBoundary = -35;

	//Assigns the current health to be the max health
	void Awake(){
		anim = GetComponent<Animator> ();
		currentHealth = maxHealth;
		currentMana = maxMana;
		levelManager = FindObjectOfType<LevelManager> ();
	}//Awake()
	
	void Update(){
			if (damaged) {
				damageImage.color = flashColour;		
			}//if
			else {
				damageImage.color = Color.Lerp (damageImage.color, Color.clear, flashSpeed * Time.deltaTime);
			}//else		

		damaged = false;

		if (currentHealth <= 0) {
			ScoreManager.score = ScoreManager.score / 2;
			levelManager.RespawnPlayer();		
			currentHealth = maxHealth;
			currentMana = maxMana;
		}//if

		if (currentHealth > maxHealth) {
			currentHealth = maxHealth;
		}//if

		if (this.transform.position.y < fallBoundary)
						DamagePlayer (9999);

		if (currentHealth == maxHealth)
			healthSlider.value = currentHealth;

		if (currentMana == maxMana)
			manaSlider.value = currentMana;

	}//if

	//damages the player and removes the appropriate health slider value
	public void DamagePlayer(int damage){
		damaged = true;
		currentHealth -= damage;
		healthSlider.value = currentHealth;
	}//DamagePlayer()

	public void manaDrain(int drain){
		//damaged = true;
		//Rigidbody2D player;
		currentMana -= drain;
		manaSlider.value = currentMana;
	}//manaDrain
	
}//Player
