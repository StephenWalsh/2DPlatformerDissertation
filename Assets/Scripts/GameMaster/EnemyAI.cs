﻿using UnityEngine;
using System.Collections;
using Pathfinding;

[RequireComponent (typeof (Rigidbody2D))]
[RequireComponent (typeof (Seeker))]
public class EnemyAI : MonoBehaviour {
	
	//Enemy AI variables
	//what to chase
	public Transform target;
	
	// update the path every 2 frames per second
	public float updateRate = 2f;
	private Seeker seeker;
	private Rigidbody2D rb;
	public Path path;
	//The AI's speed per second
	public float speed = 300f;
	public ForceMode2D fMode;
	[HideInInspector]
	public bool pathIsEnded = false;
	// The max distance from the AI to a waypoint for it to continue to the next waypoint
	public float nextWaypointDistance = 3;
	// The waypoint we are currently moving towards
	private int currentWaypoint = 0;
	private bool searchingForPlayer = false;
	private Transform enemyGraphics;
	// For determining which way the player is currently facing.
	private bool facingRight = false; 

	//Start to get components required for the enemy
	void Start () {
		seeker = GetComponent<Seeker>();
		rb = GetComponent<Rigidbody2D>();
		enemyGraphics = transform.FindChild("EnemyGraphics");

		if (target == null) {

			if (!searchingForPlayer){
				searchingForPlayer = true;
				StartCoroutine (SearchForPlayer());
			}//if
			return;
		}//if
		
		// Start a new path to the target position, return the result to the OnPathComplete method
		seeker.StartPath (transform.position, target.position, OnPathComplete);
		
		StartCoroutine (UpdatePath ());

	}//Start()

	//IEnumerator locates the search result for characters with the tag "Player"
	IEnumerator SearchForPlayer(){
		GameObject sResult = GameObject.FindGameObjectWithTag ("Player");
		//first store in a variable, then check if that variable is found (==null)or not
		if (sResult == null) {
				yield return new WaitForSeconds (0.5f);	
				StartCoroutine (SearchForPlayer ());
			}//if 
		else {
			target = sResult.transform;
			searchingForPlayer = false;

			//when you return out of a method it's going to stop going.  So updatepath will stop being called. So to fix this
			StartCoroutine(UpdatePath());
			return false;
		}//else
	}//SearchForPlayer

	//IEnumerator path allows the coroutine to take action incrementally throughout the search for the player
	IEnumerator UpdatePath () {
		if (target == null) {
			//use a start coroutine function if the enemy has lost its target and regains it target later on
			if (!searchingForPlayer){
				searchingForPlayer = true;
				StartCoroutine (SearchForPlayer());
			}//if
			return false;
		}//if
		
		// Start a new path to the target position, return the result to the OnPathComplete method
		seeker.StartPath (transform.position, target.position, OnPathComplete);
		
		yield return new WaitForSeconds ( 1f/updateRate );
		StartCoroutine (UpdatePath());
	}//UpdatePath()

	//when the path is reached by the enemy 
	public void OnPathComplete (Path p) {
		Debug.Log ("We got a path to player, checking for error: " + p.error);
		if (!p.error) {
			path = p;
			currentWaypoint = 0;
		}//if
	}//OnPathComplete
	
	void FixedUpdate () {
		//if no target, just return 
		if (target == null) {
			//if not searching for player, search for player via the StartCoroutine function
			if (!searchingForPlayer){
				searchingForPlayer = true;
				StartCoroutine (SearchForPlayer());
			}//if
			return;
		}//if
		//if no path, just return 
		if (path == null)
			return;

		//if path is reached, just return 
		if (currentWaypoint >= path.vectorPath.Count) {
			if (pathIsEnded)
				return;
			
			Debug.Log ("End of path reached.");
			pathIsEnded = true;
			return;
		}//if
		pathIsEnded = false;
		
		//Direction to the next waypoint
		Vector3 direction = (path.vectorPath[currentWaypoint] - transform.position).normalized;
		direction *= speed * Time.fixedDeltaTime;
		
		//this moves the AI in a certain direction and force
		rb.AddForce (direction, fMode);
		
		float dist = Vector3.Distance (transform.position, path.vectorPath[currentWaypoint]);
		if (dist < nextWaypointDistance) {
			currentWaypoint++;
			return;
		}
		//If the input is moving the player right and the player is facing left...
		if (direction.x > 0 && !facingRight)
			// ... flip the player.
			Flip ();
		// Otherwise if the input is moving the player left and the player is facing right...
		else if (direction.x < 0 && facingRight)
			// ... flip the player.
			Flip ();
	}//FixedUpdate()


	private void Flip()	{
		// Switch the way the player is labelled as facing.
		facingRight = !facingRight;
		
		// Multiply the player's x local scale by -1.
		Vector3 theScale = enemyGraphics.localScale;
		theScale.x *= -1;
		enemyGraphics.localScale = theScale;
	}//Flip()
	
}//EnemyAI
