﻿using UnityEngine;
using System.Collections;

public class GameMaster : MonoBehaviour {


	//GameMaster variables
	public static GameMaster gm;
	public GameObject respawnParticle;
	public int max = 100;
	private Player maxHealth;
	public Transform playerPrefab;
	public Transform spawnPoint;
	public int spawnDelay = 4;

	//Sets up componant of a null game master with tag GM
	void Awake () {
		if (gm == null) {
			gm = GameObject.FindGameObjectWithTag ("GM").GetComponent<GameMaster>();
		}//if
	}//Awake()

	//Respawns player and instantiates the respawn particle effect
	public IEnumerator RespawnPlayer () {
		yield return new WaitForSeconds (spawnDelay);
		
		Instantiate (playerPrefab, spawnPoint.position, spawnPoint.rotation);
		Instantiate (respawnParticle, spawnPoint.position, spawnPoint.rotation);
	}//RespawnPlayer()

	//destroys the player
	public static void KillPlayer (Player player) {
		Destroy (player.gameObject);
		gm.StartCoroutine (gm.RespawnPlayer());
	}//KillPlayer()

	//destroys the enemy
	public static void KillEnemy (EnemyFly enemy){
		Destroy (enemy.gameObject);
	}//KillEnemy()

	//destroys the enemy boss
	public static void KillEnemyBoss (EnemyBoss boss){
		Destroy (boss.gameObject);
	}//KillEnemyBoss()



}//GameMaster


