﻿using UnityEngine;
using System.Collections;

public class CameraShake : MonoBehaviour {


	//CameraShake variables
	public Camera mainCam;
	float shakeAmount = 0;

	//better to use Awake than start to build camera references
	void Awake (){
		if (mainCam == null) {
			mainCam = Camera.main;
			}//if
	}//Awake()

	public void Shake(float amount, float length){
		shakeAmount = amount;
		//everytime this is called, do a shaking loop
		InvokeRepeating ("BeginShake", 0, 0.01f);
		Invoke ("StopShake", length);
	}//Shake


	//BeginShake method initialises the camera shake when shot is fired
	void BeginShake(){
		if (shakeAmount > 0) {
			//creating a temp variable that we're setting equal to the mainCam.transofrm.position
			//i.e. the mainCams original position
			Vector3 camPos = mainCam.transform.position;
			//could use any value, but this looks nice in game
			//two variables that are randomised shake for x and y axis
			float shakeAmountX = Random.value * shakeAmount * 2 - shakeAmount;
			float shakeAmountY = Random.value * shakeAmount * 2 - shakeAmount;
			camPos.x += shakeAmountX;
			camPos.y += shakeAmountY;

			//upodates position when calculation is done
			mainCam.transform.position = camPos;
		}//if
	}//BeginShake

	//this method stops the camera shake
	//it does this cancelling the invoke made by the BeginShake method
	//then setting the mainCamera position equal to the camera position which is (0, 0, 0)
	void StopShake(){
		CancelInvoke ("BeginShake");
		mainCam.transform.localPosition = Vector3.zero;
	}//StopShake()

}//CameraShake
